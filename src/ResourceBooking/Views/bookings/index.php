<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<?php $this->layout('layouts::' . $theme); ?>

<h1>Resource Booking</h1>
<?=$this->alerts($messages); ?>

<div id="calendar"></div>

<!-- Modal -->
<div id="modalContainer">

</div>



<?php $this->start('scripts'); ?>
<?php $button = $acl->isAllowed($user, 'booking', 'add') ? 'bookResources' : '' ; ?>
<script>
var idleTimer;
$(function () {
    
    <?php if ($auth->isValid()) : ?>
    idleTimer = setTimeout(idleCheck, <?=$max_idle; ?>);
    <?php endif; ?>
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        schedulerLicenseKey: '0090941871-fcs-1645633266',
        //schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source', // when done
        //initialView: 'dayGridMonth',
        initialDate: '<?=$this->e($date); ?>',
        initialView: 'resourceTimelineMonth',
        resourceGroupField: 'group',
        themeSystem: 'bootstrap',
        height: 700,
        views: {
            'resourceTimelineMonth': {
                slotLabelFormat: [
                    { month: 'long', year: 'numeric' }, 
                    { day: 'numeric'} 
                    
                ]
            }
        },


        resources: '<?=$router->urlFor('resource-json'); ?>',
        events: '<?=$router->urlFor('bookings'); ?>',
        select: function(info) {
            
            let href = '<?=$router->urlFor('add-booking'); ?>?start=' + info.startStr + '&end=' + info.endStr + '&resource=' + info.resource.id;
            $.getJSON(href, function (data) {
                if (data.auth) {
                    clearTimeout(idleTimer);
                    idleTimer = setTimeout(idleCheck, <?=$max_idle; ?>);
                }
                if (! data.success) {
                    Swal.fire(
                        'Uh oh!',
                        data.message,
                        'error'
                    );
                    return;
                }
                $("#modalContainer").html(data.html)
                $('#bookingModal').modal('show');

            });
            
        },
        customButtons: {
            bookResources: {
                text: 'Toggle Resource Booking',
                click: function () {
                    if (calendar.getOption('selectable') === true) {
                        calendar.setOption('selectable', false);
                        $(this).addClass('btn-primary');
                        $(this).removeClass('btn-success');
                        Swal.fire({
                            position: 'top-end',
                            text: 'Booking Disabled',
                            showConfirmButton: false,
                            timer: 2000
                        });
                    } else {
                        // Check if auth ok.
                        $.get('<?=$router->urlFor('auth-check'); ?>', function (data) {
                            if (! data.valid) {
                                Swal.fire({
                                    title: 'Uh oh!',
                                    text: 'Your session has expired. You need to log in again',
                                    icon: 'error'
                                }).then(function (result) {
                                    window.location = '<?=$router->urlFor('home'); ?>';
                                });
                            }
                        });
                        calendar.setOption('selectable', true);
                        $(this).addClass('btn-success');
                        $(this).removeClass('btn-primary');
                        Swal.fire({
                            position: 'top-end',
                            text: 'Booking Enabled',
                            showConfirmButton: false,
                            timer: 2000
                        });
                    }
                }

            }
        },
        
        headerToolbar: { 
                left: 'title',
                center: '<?=$button; ?>',
                right: 'today,prev,next'
        },
       
        
        eventDidMount: function(info) {
            tippy(info.el, {
                content: info.event.extendedProps.bookingTitle,
                followCursor: true
            });
        },
        eventClick: function (info) {
            let id_full = info.event.id;
            let id = id_full.replace('booking-', '');
            let href = '<?=$router->urlFor('booking'); ?>?id=' + id;
            $.getJSON(href, function (data) {
                if (data.auth) {
                    clearTimeout(idleTimer);
                    idleTimer = setTimeout(idleCheck, <?=$max_idle; ?>);
                }
                $("#modalContainer").html(data.html)
                $('#bookingModal').modal('show');

            });
        },

    });
    

    calendar.render();
    calendar.on('datesSet', function () {
        $.get('<?=$router->urlFor('auth-check'); ?>', function (data) {
            if (data.valid) {
                console.log('auth ok');
                clearTimeout(idleTimer);
                idleTimer = setTimeout(idleCheck, <?=$max_idle; ?>);
            }
        });
    });

    $("#modalContainer").on('submit', '#booking-form', function (e) {
        e.preventDefault();
        let href = $(this).prop('action');
        let form_data = $(this).serialize();

        $.post(href, form_data, function (data) {
            if (data.auth) {
                clearTimeout(idleTimer);
                idleTimer = setTimeout(idleCheck, <?=$max_idle; ?>);
            }
            if (data.success) {
                Swal.fire(
                    'Success',
                    data.message,
                    'success'
                );
                $("#bookingModal").modal('hide');
                calendar.setOption('selectable', false);
                $(".fc-bookResources-button").addClass('btn-primary');
                $(".fc-bookResources-button").removeClass('btn-success');
                calendar.refetchEvents();
            } else {
                Swal.fire(
                    'Uh oh!',
                    data.message,
                    'error'
                );
            }
        });
    });
    $("#modalContainer").on('click', '#editButton', function (e) {
        e.preventDefault();

        let href = $(this).attr('href');
        $('#bookingModal').modal('hide');
        

        $('#bookingModal').on('hidden.bs.modal', function (event) {
            $("#modalContainer").html('')
            
            $.get(href, function (data) {
                
                $("#modalContainer").html(data.html)

                $('#bookingModal').modal('show');
            });
        })
    });

    $(".chip .close").on('click', function () {
        $(this).parent().hide();

    });
    function idleCheck() {
        Swal.fire({
            icon: "info",
            text: "Your session may have expired. Please login again if you want to add or modify bookings."
        }).then(function (result) {
            window.location = '<?=$router->urlFor('home'); ?>';
        });
    }
    
    


});
</script>
<?php $this->end(); ?>