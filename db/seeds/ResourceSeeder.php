<?php


use Phinx\Seed\AbstractSeed;

class ResourceSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $handle = fopen(dirname(__DIR__) . "/seeds/resources.csv", "r");

        

        $rows = [];
        if ($handle !== false) {
            while (($row = fgetcsv($handle)) !== false) {
                $rows[] = [
                    'id'=>$row[0],
                    'resource'=>$row[1],
                    'resource_group_id'=>$row[2],
                    'model'=>$row[3] == 'NULL' ? null : $row[3],
                    'serial_number'=>$row[4] == 'NULL' ? null : $row[4],
                    'location'=>$row[5],
                    'description'=>$row[6] == 'NULL' ? null : $row[6],
                    'image'=>$row[7] == 'NULL' ? null : $row[7],
                    'current_mileage'=>$row[8] == 'NULL' ? null : $row[8],
                    'service_mileage'=>$row[9] == 'NULL' ? null : $row[9],
                    'service_date'=>$row[10] == 'NULL' ? null : $row[10],
                    'available'=>$row[11],
                ];
            }
        }

        fclose($handle);

        $resource = $this->table('resource');
        $resource->insert($rows)
              ->save();
    }

    public function getDependencies()
    {
        return [
            'ResourceGroupSeeder',
        ];
    }
}
