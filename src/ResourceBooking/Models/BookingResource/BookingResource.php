<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace ResourceBooking\Models\BookingResource;

use Atlas\Mapper\Mapper;
use Atlas\Table\Row;

/**
 * @method BookingResourceTable getTable()
 * @method BookingResourceRelationships getRelationships()
 * @method BookingResourceRecord|null fetchRecord($primaryVal, array $with = [])
 * @method BookingResourceRecord|null fetchRecordBy(array $whereEquals, array $with = [])
 * @method BookingResourceRecord[] fetchRecords(array $primaryVals, array $with = [])
 * @method BookingResourceRecord[] fetchRecordsBy(array $whereEquals, array $with = [])
 * @method BookingResourceRecordSet fetchRecordSet(array $primaryVals, array $with = [])
 * @method BookingResourceRecordSet fetchRecordSetBy(array $whereEquals, array $with = [])
 * @method BookingResourceSelect select(array $whereEquals = [])
 * @method BookingResourceRecord newRecord(array $fields = [])
 * @method BookingResourceRecord[] newRecords(array $fieldSets)
 * @method BookingResourceRecordSet newRecordSet(array $records = [])
 * @method BookingResourceRecord turnRowIntoRecord(Row $row, array $with = [])
 * @method BookingResourceRecord[] turnRowsIntoRecords(array $rows, array $with = [])
 */
class BookingResource extends Mapper
{
}
