<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<?php $this->layout('layouts::' . $theme) ?>

<?=$this->alerts($messages); ?>

<h2>Login</h2>

<?php if (! $auth->isValid()) : ?>

<div class="d-flex d-flex justify-content-center">
    <div class=" border p-2 w-25 bg-light">
    <?php if ($network_policy_string) : ?>
    <p><?php printf($network_policy_string, $network_policy_url); ?></p>
    <?php endif; ?>
    <?=$form->begin(); ?>
    <?=$form->hidden('csrf_token')->idName('csrf_token')->value($csrf_token); ?>
    
        <div class="form-group">
            <?=$form->label('Username')->for('username'); ?>
            <?=$form->text()->idName('username')->class('form-control'); ?>
        </div>
        <div class="form-group">
            <?=$form->label('Password')->for('password'); ?>
            <?=$form->password()->idName('password')->class('form-control'); ?>
        </div>

    <?=$form->submit('Login')->class('btn btn-success'); ?>
    <?=$form->end(); ?>
    </div>
</div>
<?php else : ?>
<p>You are already logged in.</p>
<?php endif; ?>