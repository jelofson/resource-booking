<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class UserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        if ( ! $this->hasTable('user')) {
            $table = $this->table('user');
            $table->addColumn('first_name', 'string', ['limit'=>25])
                  ->addColumn('last_name', 'string', ['limit'=>25])
                  ->addColumn('email', 'string', ['limit'=>100])
                  ->addColumn('userid', 'string', ['limit'=>25])
                  ->addColumn('password', 'string', ['limit'=>255, 'null'=>true])
                  ->addColumn('role', 'string', ['limit'=>15, 'default'=>'user'])
                  ->addColumn('active', 'integer', ['default'=>1, 'signed'=>false])
                  ->addIndex('userid')->addIndex('email')
                  ->create();
        }
    }
}
