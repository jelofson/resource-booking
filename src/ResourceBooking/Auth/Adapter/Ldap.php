<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ResourceBooking\Auth\Adapter;

use Vespula\Auth\Adapter\Ldap as VespulaLdap;
use Atlas\Orm\Atlas;
use ResourceBooking\Models\User\User;

class Ldap extends VespulaLdap
{
    protected $atlas;


    public function setAtlas(Atlas $atlas)
    {
        $this->atlas = $atlas;
    }


    public function syncDb($userdata)
    {
        $userid = $userdata['username'];
        $email = $userdata['email'];
        $first_name = $userdata['first_name'];
        $last_name = $userdata['last_name'];
        

        $user = $this->atlas->select(User::class)
            ->where('userid = ', $userid)
            ->fetchRecord();
        
        if ($user) {
            // Update the data
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $this->atlas->update($user);
            return;
        } 

        // Insert new record
        $user = $this->atlas->newRecord(User::class);
        $user->userid = $userid;
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = $email;
        $user->role = 'user';
        $user->active = 1;
        $this->atlas->insert($user);
        return;
    }
}