<?php


use Phinx\Seed\AbstractSeed;

class BookingResourceSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $handle = fopen(dirname(__DIR__) . "/seeds/resource_bookings_resources.csv", "r");

        $rows = [];
        if ($handle !== false) {
            while (($row = fgetcsv($handle)) !== false) {
                $rows[] = [
                    'id'=>$row[0],
                    'booking_id'=>$row[1],
                    'resource_id'=>$row[2],
                ];
            }
        }

        fclose($handle);

        $booking = $this->table('booking_resource');
        $booking->insert($rows)
              ->save();
    }

    public function getDependencies()
    {
        return [
            'BookingSeeder',
            'ResourceSeeder'
        ];
    }
}
