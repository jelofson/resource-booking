<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */


namespace ResourceBooking\Controllers;

use DateTime;
use Psr\Log\NullLogger;
use ResourceBooking\Models\Booking\Booking;
use ResourceBooking\Models\BookingResource\BookingResource;
use ResourceBooking\Models\Resource\Resource;
use ResourceBooking\Models\ResourceGroup\ResourceGroup;
use ResourceBooking\Models\User\User;

class Bookings extends Controller
{
    protected $mapper_class;

    const ACTION_EDIT = 'EDIT';
    const ACTION_ADD = 'ADD';

    public function index()
    {
        
        $date = filter_input(INPUT_GET, 'date', FILTER_SANITIZE_STRING);
        if (! preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date)) {
            $date = date('Y-m-d');
        }
        $max_idle = $this->auth->getSession()->getIdle() * 1000; // in seconds
        //$max_idle = 30000;
        $content = $this->view->render('bookings/index', [
            'messages'=>$this->messages,
            'date'=>$date,
            'max_idle'=>$max_idle 
        ]);

        

        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function bookings()
    {
        $start = filter_input(INPUT_GET, 'start', FILTER_SANITIZE_STRING);
        $end = filter_input(INPUT_GET, 'end', FILTER_SANITIZE_STRING);

        if (! $start) {
            $start = date('Y-m') . "-01";
        }

        if (! $end) {
            $end = date('Y-m-t');
        }

        

        $this->select->reset();
        $this->select->columns('r.id, r.resource, b.id as booking_id, b.title, b.date_start, b.date_end, b.userid, b.returned')
            ->from('resource r')
            ->join('INNER', 'booking_resource br', 'r.id = br.resource_id')
            ->join('INNER', 'booking b', 'b.id = br.booking_id')
            ->catJoin(' AND (b.date_start between :start and :end 
                        OR b.date_end between :start and :end 
                        OR :start between b.date_start and b.date_end 
                        OR :end between b.date_start and b.date_end)'
            )
            ->bindValues([
                'start'=>$start,
                'end'=>$end,
            ]);

        $result = $this->select->fetchAll();
        $today = new \DateTime();

        $data = [];
        if ($result) {
            foreach ($result as $row) {
                
                $end = new \DateTime($row['date_end']);
                $color = '';
                $bgcolor = '';
                if ($row['returned'] != "1" && $end <= $today) {
                    $bgcolor = '#ffc107';
                    $color = '#000';
                }
                if ($row['returned'] == "1") {
                    $bgcolor = '#3cb371';
                    $color = '#fff';
                }

                $data[] = [
                    'id'=>'booking-' . $row['booking_id'],
                    'resourceId'=>$row['id'],
                    'title'=>$row['resource'],
                    'start'=>$row['date_start'],
                    'end'=>$row['date_end'],
                    'allDay'=>true,
                    'backgroundColor'=>$bgcolor,
                    'borderColor'=>$bgcolor,
                    'textColor'=>$color,
                    'extendedProps'=>[
                        'bookingTitle'=>$row['title'] . ' (' . $row['userid'] . ')',
                    ]
                ];
                
            }
        }
        
        $this->response->getBody()->write(json_encode($data));

        return $this->response->withHeader('Content-type', 'application/json');

    }

    public function booking()
    {
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);

        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'bookingResources'=>[
                'resource'
            ],
            'user'
        ]);

        if (! $booking) {
            $content = [
                'success'=>false,
                'code'=>404,
                'message'=>'Could not find that booking'
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }

        $content = $this->view->render('bookings/modal', [
            'booking'=>$booking,
        ]);


        $data = [
            'success'=>true,
            'auth'=>$this->auth->isValid(),
            'booking'=>$booking,
            'html'=>$content,
            
        ];

        $this->response->getBody()->write(json_encode($data));
        return $this->response->withHeader('Content-type', 'application/json');
        
    }

    public function return($id)
    {
        
        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'user',
            'bookingResources'=>[
                'resource'=>[
                    'group'
                ]
            ]
        ]);

        if (! $booking) {
            return $this->notFound();
        }
        if (! $this->acl->isAllowed($this->user, $booking, 'return')) {
            return $this->notAllowed();
        }

        $booking->returned = 1;
        $booking->returned_date = date('Y-m-d');
        $booking->returned_by = $this->user->getUsername();
        $booking->updated_by = $this->user->getUsername();

        try {
            $this->atlas->update($booking);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->trigger('return-error', $booking, $message);
            return $this->redirect($this->router->urlFor('home'));
        }
        $this->trigger('returned', $booking);
        return $this->redirect($this->router->urlFor('home') . '?date=' . $booking->date_start);

    }

    public function unreturn($id)
    {
        
        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'user',
            'bookingResources'=>[
                'resource'=>[
                    'group'
                ]
            ]
        ]);

        if (! $booking) {
            return $this->notFound();
        }
        if (! $this->acl->isAllowed($this->user, $booking, 'unreturn')) {
            return $this->notAllowed();
        }

        $booking->returned = 0;
        $booking->returned_date = null;
        $booking->returned_by = null;
        $booking->updated_by = $this->user->getUsername();

        try {
            $this->atlas->update($booking);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->trigger('unreturn-error', $booking, $message);
            return $this->redirect($this->router->urlFor('home'));
        }
        $this->trigger('unreturned', $booking);
        return $this->redirect($this->router->urlFor('home') . '?date=' . $booking->date_start);

    }

    public function add()
    {
        if (! $this->isXhr()) {
            return $this->badRequest('Invalid request');
        }
        
        $start = filter_input(INPUT_GET, 'start', FILTER_SANITIZE_STRING);
        $end = filter_input(INPUT_GET, 'end', FILTER_SANITIZE_STRING);
        $resource_id = filter_input(INPUT_GET, 'resource', FILTER_SANITIZE_STRING);

        $booking = $this->atlas->newRecord(Booking::class, [
            'date_start'=>$start,
            'date_end'=>$end,
        ]);

        if (! $this->acl->isAllowed($this->user, $booking, 'add')) {
            $data = [
                'success'=>false,
                'status'=>403,
                'message'=>'Not Allowed (maybe your session expired)'
            ];
            $this->response->getBody()->write(json_encode($data));
            return $this->response->withHeader('Content-type', 'application/json');
        }

        $booking->userid = $this->user->getUsername();

        $bookingResources = $this->atlas->newRecordSet(BookingResource::class);
        $bookingResources->appendNew([
            'resource_id'=>$resource_id
        ]);
        $booking->bookingResources = $bookingResources;

        $html = $this->buildForm($booking->getArrayCopy(), self::ACTION_ADD);

        $content = [
            'success'=>true,
            'auth'=>$this->auth->isValid(),
            'status'=>200,
            'html'=>$html
        ];
        $this->response->getBody()->write(json_encode($content));
        return $this->response->withHeader('Content-type', 'application/json');
    }

    public function doAdd()
    {
        if (! $this->isXhr()) {
            return $this->badRequest('Invalid request');
        }

        $data = $this->sanitizePost();

        $csrf_token = $this->session->getCsrfToken();
        if (! $csrf_token->isValid($data['csrf_token'])) {
            throw new \Exception('Invalid token. Perhaps your session expired, or you are trying something your should not?');
        }

        $booking = $this->atlas->newRecord(Booking::class, [
            'title'=>$data['title'],
            'date_start'=>$data['date_start'],
            'date_end'=>$data['date_end'],
            'userid'=>$data['userid'],
            'created_by'=>$this->user->getUsername(),
            'updated_by'=>$this->user->getUsername(),
            'returned'=>0
        ]);

        if (! $this->acl->isAllowed($this->user, $booking, 'add')) {
            $data = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>403,
                'message'=>'Not Allowed (maybe your session expired)'
            ];
            $this->response->getBody()->write(json_encode($data));
            return $this->response->withHeader('Content-type', 'application/json');
        }

        $available_resource_ids = $this->getAvailableResources($data['date_start'], $data['date_end']);
        $resource_error = false;
        foreach ($data['resources'] as $resource_id) {
            if (! in_array($resource_id, $available_resource_ids)) {
                $resource_error = true;
                break;
            }
        }

        if ($resource_error) {
            $content = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>500,
                'message'=>'One or more of your selected resources is not available for the selected dates'
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }

        $booking->validate();
        if (! $booking->isValid()) {
            $message = "";
            foreach ($booking->getErrors() as $error) {
                $message .= $error . "<br/>\n";
            }
            $content = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>200,
                'message'=>$message
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }

        try {
            $this->atlas->insert($booking);
        } catch (\Exception $e) { 
            $this->trigger('add-booking-error', $e->getMessage());
            $content = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>500,
                'message'=>$e->getMessage()
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }


        foreach ($data['resources'] as $resource_id) {
            $resourceBooking = $this->atlas->newRecord(BookingResource::class, [
                'booking_id'=>$booking->id,
                'resource_id'=>$resource_id
            ]);
            $this->atlas->insert($resourceBooking); 
        }
        $info = [
            'success'=>true,
            'auth'=>$this->auth->isValid(),
            'status'=>200,
            'message'=>'Booking created'
        ];

        $this->trigger('add-booking', $booking, $data['resources']);

        $this->response->getBody()->write(json_encode($info));
        return $this->response->withHeader('Content-type', 'application/json');
    }

    public function edit($id)
    {
        if (! $this->isXhr()) {
            return $this->badRequest('Invalid request');
        }

        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'bookingResources'
        ]);

        if (! $this->acl->isAllowed($this->user, $booking, 'edit')) {
            $data = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>403,
                'message'=>'Unauthorized'
            ];
            $this->response->getBody()->write(json_encode($data));
            return $this->response->withStatus(403)->withHeader('Content-type', 'application/json');
        }

        $html = $this->buildForm($booking->getArrayCopy(), self::ACTION_EDIT);

        $content = [
            'success'=>true,
            'auth'=>$this->auth->isValid(),
            'status'=>200,
            'html'=>$html
        ];
        $this->response->getBody()->write(json_encode($content));
        return $this->response->withHeader('Content-type', 'application/json');
    }

    public function doEdit($id)
    {
        if (! $this->isXhr()) {
            return $this->badRequest('Invalid request');
        }

        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'bookingResources'=>[
                'resource'=>[
                    'group'
                ]
            ]
        ]);

        if (! $this->acl->isAllowed($this->user, $booking, 'edit')) {
            $data = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>403,
                'message'=>'Unauthorized'
            ];
            $this->response->getBody()->write(json_encode($data));
            return $this->response->withStatus(403)->withHeader('Content-type', 'application/json');
        }

        $data = $this->sanitizePost();

        $csrf_token = $this->session->getCsrfToken();
        if (! $csrf_token->isValid($data['csrf_token'])) {
            throw new \Exception('Invalid token. Perhaps your session expired, or you are trying something your should not?');
        }

        $booking->title = $data['title'];
        $booking->date_start = $data['date_start'];
        $booking->date_end = $data['date_end'];
        $booking->userid = $data['userid'];
        $booking->returned = $data['returned'] ?? 0;
        
        $booking->updated_by = $this->user->getUsername();
        
        $available_resource_ids = $this->getAvailableResources($data['date_start'], $data['date_end'], $id);

        $resource_error = false;
        foreach ($data['resources'] as $resource_id) {
            if (! in_array($resource_id, $available_resource_ids)) {
                $resource_error = true;
                break;
            }
        }

        if ($resource_error) {
            $content = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>500,
                'message'=>'One or more of your selected resources is not available for the selected dates'
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }
        
        $booking->validate();
        if (! $booking->isValid()) {
            $message = "";
            foreach ($booking->getErrors() as $error) {
                $message .= $error . "\n";
            }
            $content = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>200,
                'message'=>$message
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }


        try {
            $this->atlas->update($booking);
        } catch (\Exception $e) {
            $this->trigger('edit-booking-error', $e->getMessage());
            $content = [
                'success'=>false,
                'auth'=>$this->auth->isValid(),
                'status'=>500,
                'message'=>$e->getMessage()
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json')->withStatus(500);
        }

        $booking->bookingResources->setDelete();
    

        foreach ($data['resources'] as $resource_id) {
            
            $booking->bookingResources->appendNew([
                'booking_id'=>$booking->id,
                'resource_id'=>$resource_id]
            ); 
        }
        $this->atlas->persistRecordSet($booking->bookingResources);
        $data = [
            'success'=>true,
            'auth'=>$this->auth->isValid(),
            'status'=>200,
            'message'=>'Booking updated'
        ];
        $this->trigger('edit-booking', $booking);

        $this->response->getBody()->write(json_encode($data));
        return $this->response->withHeader('Content-type', 'application/json');

    }

    public function delete($id)
    {
        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'bookingResources'=>[
                'resource'
            ]
        ]);

        if (! $booking) {
            return $this->notFound();
        }

        if (! $this->acl->isAllowed($this->user, $booking, 'delete')) {
            return $this->notAllowed();
        }

        $csrf_token = $this->session->getCsrfToken()->getValue();
        $cancel_href = $this->router->urlFor('home');
        $this->form->addElement('csrf_token', $this->form->hidden()->idName('csrf_token')->value($csrf_token));
        $content = $this->view->render('bookings/delete', [
            'messages'=>$this->messages,
            'booking'=>$booking,
            'form'=>$this->form,
            'cancel_href'=>$cancel_href
            
        ]);

        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function doDelete($id)
    {
        $booking = $this->atlas->fetchRecord(Booking::class, $id, [
            'bookingResources'=>[
                'resource'=>[
                    'group'
                ]
            ]
        ]);

        $csrf_token_value = filter_input(INPUT_POST, 'csrf_token', FILTER_SANITIZE_STRING);
        $csrf_token = $this->session->getCsrfToken();
        if (! $csrf_token->isValid($csrf_token_value)) {
            throw new \Exception('Invalid token. Perhaps your session expired, or you are trying something your should not?');
        }

        if (! $booking) {
            return $this->notFound();
        }

        if (! $this->acl->isAllowed($this->user, $booking, 'delete')) {
            return $this->notAllowed();
        }

        $bookingResources = $booking->bookingResources;

        try {
            foreach ($booking->bookingResources as $br) {
                $this->atlas->delete($br);
            }
            // Set to null or you get a warning about immutable ID
            $booking->bookingResources = null;
            $this->atlas->delete($booking);
            
        } catch (\Exception $e) {
            $this->trigger('delete-fail', $booking);
            $this->setFlash('error', $e->getMessage());
            return $this->redirect($this->router->urlFor('home'));
        }

        // Add these back after delete from DB.
        // Look into better way...persist?
        $booking->bookingResources = $bookingResources;
        $this->trigger('delete-booking', $booking);
        
        return $this->redirect($this->router->urlFor('home'));
    }

    /**
     * Unused for now
     */
    public function resourceSelect()
    {
        $start = filter_input(INPUT_GET, 'start', FILTER_SANITIZE_STRING);
        $end = filter_input(INPUT_GET, 'end', FILTER_SANITIZE_STRING);
        // Need to add a day to manually typed end date
        $endDate = new \DateTime($end);
        $endDate->add(new \DateInterval('P1D'));
        $end = $endDate->format('Y-m-d');
        $resources = trim(filter_input(INPUT_GET, 'resources', FILTER_SANITIZE_STRING));
        $resource_ids = explode(',', $resources);
        $available_resource_ids = $this->getAvailableResources($start, $end);

        $groups = $this->atlas->select(ResourceGroup::class)
            ->columns('id', 'resource_group')
            ->with(['resources'])
            ->orderBy('resource_group ASC')
            ->fetchRecordSet();
        

        //$available_resource_ids = array_merge($available_resource_ids, $resource_ids);

        $resourceOptions = [];

        foreach ($groups as $group) {
            $resourceOptions[$group->resource_group] = [];
            foreach ($group->resources as $resource) {
                if (in_array($resource->id, $available_resource_ids)) {
                    $resourceOptions[$group->resource_group][$resource->id] = $resource->resource;
                }
            }
        }



        $resourceSelect = $this->form->select()->id('resources')->name('resources[]')
            ->options($resourceOptions)->multiple()
            ->class('form-control')->required()
            ->selected($resource_ids);

        $this->form->addElement('resources', $resourceSelect);

        $html = $this->view->render('bookings/resource-select', [
            'form'=>$this->form
        ]);

        $data = [
            'success'=>true,
            'status'=>200,
            'html'=>$html,
            
        ];

        $this->response->getBody()->write(json_encode($data));
        return $this->response->withHeader('Content-type', 'application/json');
    }

    public function checkOverdue()
    {
        // Get all that are not returned
        $records = $this->atlas->select(Booking::class)
            ->where('returned != 1')
            ->with(['bookingResources'=>[
                'resource'=>[
                    'group'
                ]
            ]])
            ->fetchRecords();

        foreach ($records as $booking) {
            if ($booking->isOverdue()) {
                $this->notifyOverdue($booking);
            }
            
        }

        echo "Notices Sent";
        exit;

    }
 

    protected function buildForm($data, $action)
    {
        $groups = $this->atlas->select(ResourceGroup::class)
            ->columns('id', 'resource_group')
            ->with(['resources'])
            ->orderBy('resource_group ASC')
            ->fetchRecordSet();

        $available_resource_ids = $this->getAvailableResources($data['date_start'], $data['date_end']);
        
        //print_r($available_resource_ids); exit;
        

        $date_end = new \DateTime($data['date_end']);
        $date_end->sub(new \DateInterval('P1D'));
        $data['date_end'] = $date_end->format('Y-m-d');

        $columnOptions = [];
        
        $columnOptions = [
            'date_start'=>[
                'type'=>'date'
            ],
            'date_end'=>[
                'type'=>'date'
            ],
            'userid'=>[
                'type'=>'hidden'
            ],
            'returned'=>[
                'type'=>'checkbox'
            ]
        ];
        

        $resource_ids = [];
        foreach ($data['bookingResources'] as $br) {
            $resource_ids[] = $br['resource_id'];
        }

        $available_resource_ids = array_merge($available_resource_ids, $resource_ids);

        $resourceOptions = [];

        foreach ($groups as $group) {
            $resourceOptions[$group->resource_group] = [];
            foreach ($group->resources as $resource) {
                if (in_array($resource->id, $available_resource_ids)) {
                    $resourceOptions[$group->resource_group][$resource->id] = $resource->resource;
                }
            }
        }

        foreach ($resourceOptions as $group=>$opts) {
            if (count($opts) < 1) {
                unset($resourceOptions[$group]);
            }
        }


        $resourceSelect = $this->form->select()->id('resources')->name('resources[]')
            ->options($resourceOptions)->multiple()
            ->class('form-control')->required()
            ->selected($resource_ids);

        $builder = $this->form->getBuilder();
        $builder->setColumnOptions($columnOptions);
        $this->form->build('booking', $data);
        $this->form->addElement('resources', $resourceSelect);

        $csrf_token = $this->form->hidden()->idName('csrf_token')->value($this->session->getCsrfToken()->getValue());
        $this->form->addElement('csrf_token', $csrf_token);

        switch ($action) {
            case self::ACTION_ADD :
                $form_action = $this->router->urlFor('add-booking');
                $title = 'Add Booking';
            break;
            case self::ACTION_EDIT :
                $form_action = $this->router->urlFor('edit-booking', ['id'=>$data['id']]);
                $title = 'Edit Booking';
            break;
            default :

        }

        $fullname = $data['userid'] ?? "";
        if ($data['userid']) {
            $userRecord = $this->atlas->select(User::class)
            ->where('userid = ', $data['userid'])
            ->fetchRecord();
            if ($userRecord) {
                $fullname = $userRecord->getFullName();
            }

            if (! $userRecord) {
                $this->log->error('Could not find {userid} in the employee view. Maybe a sync DB issue with vespula.employee?', [
                    'userid'=>$data['userid']
                ]);
            }
            
        }
        
        $content = $this->view->render('bookings/form-modal', [
            'messages'=>$this->messages,
            'action'=>$action,
            'form'=>$this->form,
            'form_action'=>$form_action,
            'title'=>$title,
            'fullname'=>$fullname
        ]);

        return $content;
    }

    protected function sanitizePost()
    {
        $fields = [
            'title',
            'date_start',
            'date_end',
            'userid',
            'csrf_token',
            'returned'
        ];

        $clean = [];
        foreach ($fields as $field) {
            $clean[$field] = filter_input(INPUT_POST, $field, FILTER_SANITIZE_STRING);
        }

        $date_end = new \DateTime($clean['date_end']);
        $date_end->add(new \DateInterval('P1D'));
        $clean['date_end'] = $date_end->format('Y-m-d');

        $clean['resources'] = filter_input(INPUT_POST, 'resources', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

        return $clean;
    }

    protected function getAvailableResources($start, $end, $booking_id = null)
    {
        // Could do a better query here, but it would end up being DB-specific, so I will do this
        // the long way.

        // Event dates always end on the day after the chosen end date. If you chose 2021-03-04 for an event, 
        // The dates would actually be 2021-03-04 to 2021-03-05. Need to take this into account when looking at
        // overlapping bookings.

        // Need date objects for testing later. Start by decreasing the end date by one day.
        $startDate = new \DateTime($start);
        $endDate = new \DateTime($end);
        $endDate->sub(new \DateInterval('P1D'));
        $end_modified = $endDate->format('Y-m-d');
        

        //get already booked resources
        $select = $this->atlas->select(Booking::class)
            ->columns('*')
            ->with([
                'bookingResources'
            ])
            ->where('returned = 0')
            ->andWhereSprintf('( date_start between %s and %s ', $start, $end_modified)
            ->orWhereSprintf('date_end between %s and %s ', $start, $end_modified)
            ->orWhereSprintf('%s between date_start and date_end', $start) 
            ->orWhereSprintf('%s between date_start and date_end )', $end_modified);
        
        if ($booking_id) {
            $select->andWhere('id != ', $booking_id);
        }    
        $rows = $select->fetchRecordSet();

        
        // loop through the rows and adjust the end date to be one day less, and then check for overlap again
        
        $resource_ids = [];
        
        foreach ($rows as $booking) {

            $bookingStartDate = new \DateTime($booking->date_start);
            $bookingEndDate = new \DateTime($booking->date_end);
            $bookingEndDate->sub(new \DateInterval('P1D'));
            
            $firstCondition = $startDate >= $bookingStartDate && $startDate <= $bookingEndDate;
            $secondCondition = $endDate >= $bookingStartDate && $endDate <= $bookingEndDate;
            $thirdCondition = $bookingStartDate >= $startDate && $bookingStartDate <= $endDate;
            $fourthCondition = $bookingEndDate >= $startDate && $bookingEndDate <= $endDate;
            if (($firstCondition) || ($secondCondition) || ($thirdCondition) || ($fourthCondition)) {
                foreach ($booking->bookingResources as $br) {
                    $resource_ids[] = $br->resource_id;
                }
            }
        }
        
        $select = $this->atlas->select(Resource::class)
            ->columns('id')
            ->where('available = 1')
            ->orderBy('id');
        
        if ($resource_ids) {
            $select->where('id NOT IN ', $resource_ids);
        }

        return $select->fetchColumn();
    }

    protected function notifyOverdue($booking)
    {
        $settings = $this->settings;
        $userids = [];
        $emails = [];

        $booker_userid = $booking->userid;
        $emp_record = $this->getEmployee($booker_userid);
        $fullname = $booker_userid;
        if ($emp_record) {
            $emails[] = $emp_record->email;
            $fullname = $emp_record->getFullName(false);
        }

        foreach ($booking->bookingResources as $br) {
            $userid_string = $br->resource->group->userid;
            if ($userid_string) {
                $userids = array_merge($userids, explode(',', $userid_string));
            }
        }

        $userids = array_unique($userids);
        
        foreach ($userids as $userid) {
            $emp_record = $this->getEmployee($userid);
            if ($emp_record) {
                $emails[] = $emp_record->email;
            }
        }

        $uri = $this->getBookingUrl();

        $emails = array_unique($emails);
        $subject = "Resources Due";
        $this->mailMessage->addTo($emails);
        $this->mailMessage->setSubject($subject);
        $this->mailMessage->setFrom($settings['email_from']);
        $body = $this->view->render('bookings/email-overdue', [
            'fullname'=>$fullname,
            'booking'=>$booking,
            'uri'=>$uri
        ]);


        $this->mailMessage->setBody($body);
        $this->mailTransport->send($this->mailMessage);
    }

    protected function notifyReturned($booking)
    {
        $settings = $this->settings;
        $userids = [];
        // Get the people responsible for the resources
        foreach ($booking->bookingResources as $br) {
            $userid_string = $br->resource->group->userid;
            if ($userid_string) {
                $userids = array_merge($userids, explode(',', $userid_string));
            }
        }

        $userids = array_unique($userids);
        $emails = [];
        foreach ($userids as $userid) {
            $emp_record = $this->getEmployee($userid);
            if ($emp_record) {
                $emails[] = $emp_record->email;
            }
        }

        // Get the person who booked it
        
        $emails[] = $booking->user->email;
        

        $fullname = $this->user->getUsername();
        $returner_userid = $this->user->getUsername();
        $emp_record = $this->getEmployee($returner_userid);
        if ($emp_record) {
            $last_first = false;
            $fullname = $emp_record->getFullName($last_first);
        }

        $emails = array_unique($emails);
        $subject = "Resources Returned";
        $this->mailMessage->addTo($emails);
        $this->mailMessage->setSubject($subject);
        $this->mailMessage->setFrom($settings['email_from']);
        $body = $this->view->render('bookings/email-returned', [
            'fullname'=>$fullname,
            'resources'=>$booking->getResourcesAsString("\n", " * "),
            'title'=>$booking->title,
        ]); 

        $this->mailMessage->setBody($body);
        $this->mailTransport->send($this->mailMessage);


    }

    protected function notifyDeleted($booking)
    {
        $settings = $this->settings;
        $userids = [];
        // Get the people responsible for the resources
        foreach ($booking->bookingResources as $br) {
            $userid_string = $br->resource->group->userid;
            if ($userid_string) {
                $userids = array_merge($userids, explode(',', $userid_string));
            }
        }

        $userids = array_unique($userids);
        $emails = [];
        foreach ($userids as $userid) {
            $emp_record = $this->getEmployee($userid);
            if ($emp_record) {
                $emails[] = $emp_record->email;
            }
        }

        // Get the person who booked it
        $booker_userid = $booking->userid;
        $emp_record = $this->getEmployee($booker_userid);
        if ($emp_record) {
            $emails[] = $emp_record->email;
        }

        $fullname = $this->user->getUsername();
        $returner_userid = $this->user->getUsername();
        $emp_record = $this->getEmployee($returner_userid);
        if ($emp_record) {
            $last_first = false;
            $fullname = $emp_record->getFullName($last_first);
        }

        $emails = array_unique($emails);
        $subject = "Booking Deleted";
        $title = $booking->title;
        $this->mailMessage->addTo($emails);
        $this->mailMessage->setSubject($subject);
        $this->mailMessage->setFrom($settings['email_from']);

        $body = $this->view->render('bookings/email-delete', [
            'fullname'=>$fullname,
            'title'=>$title,
            'resources'=>$booking->getResourcesAsString("\n", " * ")
        ]);

        $this->mailMessage->setBody($body);
        $this->mailTransport->send($this->mailMessage);

    }

    protected function notifyBooked($booking, $resources)
    {
        $settings = $this->settings;
        $userids = [];
        // Get the people responsible for the resources
        foreach ($resources as $resource) {
            $userid_string = $resource->group->userid;
            if ($userid_string) {
                $userids = array_merge($userids, explode(',', $userid_string));
            }
        }

        $userids = array_unique($userids);
        $emails = [];
        foreach ($userids as $userid) {
            $emp_record = $this->getEmployee($userid);
            if ($emp_record) {
                $emails[] = $emp_record->email;
            }
        }

        // Get the person who booked it
        $booker_userid = $booking->userid;
        $emp_record = $this->getEmployee($booker_userid);
        if ($emp_record) {
            $emails[] = $emp_record->email;
        }

        $fullname = $this->user->getUsername();
        $booker_userid = $this->user->getUsername();
        $emp_record = $this->getEmployee($booker_userid);
        if ($emp_record) {
            $last_first = false;
            $fullname = $emp_record->getFullName($last_first);
        }

        $uri = $this->getBookingUrl();

        $startDate = new \DateTime($booking->date_start);
        $endDate = new \DateTime($booking->date_end);
        $endDate->sub(new \DateInterval("P1D"));

        $emails = array_unique($emails);
        $subject = "Resources Booked";
        $this->mailMessage->addTo($emails);
        $this->mailMessage->setSubject($subject);
        $this->mailMessage->setFrom($settings['email_from']);
        $body = $this->view->render('bookings/email-booked', [
            'fullname'=>$fullname,
            'uri'=>$uri,
            'resources'=>$resources,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
        ]);


        $this->mailMessage->setBody($body);
        $this->mailTransport->send($this->mailMessage);


    }

    protected function getEmployee($userid)
    {
        $user = $this->atlas->select(User::class)
            ->columns('id', 'first_name', 'last_name', 'email', 'userid', 'active')
            ->where('userid = ', $userid)
            ->fetchRecord();
        

        return $user;
    }
    protected function registerEvents()
    {
        $this->on('returned', function ($booking) {
            $this->notifyReturned($booking);
            $resources = $booking->getResourcesAsString();
            $this->setFlash('info', 'Resources Returned');
            $this->log->info('Resources [{resources}] on booking {id} returned by {userid}', [
                'resources'=>$resources,
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('return-error', function ($booking, $message) {
            
            $resources = $booking->getResourcesAsString();
            $this->setFlash('error', 'Resources NOT Returned. ' . $message);
            $this->log->error('Resources [{resources}] on booking {id} NOT returned by {userid}. Error: {message}', [
                'message'=>$message,
                'resources'=>$resources,
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('unreturned', function ($booking) {
            //$this->notifyReturned($booking);
            $resources = $booking->getResourcesAsString();
            $this->setFlash('info', 'Resources marked as not returned');
            $this->log->info('Resources [{resources}] on booking {id} marked not returned by {userid}', [
                'resources'=>$resources,
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('unreturn-error', function ($booking, $message) {
            
            $resources = $booking->getResourcesAsString();
            $this->setFlash('error', 'Resources NOT marked as not returned. ' . $message);
            $this->log->error('Resources [{resources}] on booking {id} NOT marked not returned by {userid}. Error: {message}', [
                'message'=>$message,
                'resources'=>$resources,
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('add-booking', function ($booking, $resource_ids) {
            $resources = $this->atlas->fetchRecordSet(Resource::class, $resource_ids, [
                'group'
            ]);
            $this->notifyBooked($booking, $resources);
            
            $this->setFlash('info', 'Resources Returned');
            $this->log->info('Resources [{resources}] on booking {id} returned by {userid}', [
                'resources'=>$resources->asString(),
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('delete-booking', function ($booking) {
            
            $this->notifyDeleted($booking);
            $resources = $booking->getResourcesAsString();
            $this->setFlash('info', 'Booking Deleted');

            $this->log->info('Booking {id} with resources [{resources}] deleted by {userid}', [
                'resources'=>$resources,
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('edit-booking', function ($booking) {
            
            //$this->notifyEdit($booking);
            $resources = $booking->getResourcesAsString();
            $this->log->info('Booking {id} with resources [{resources}] modified by {userid}', [
                'resources'=>$resources,
                'userid'=>$this->user->getUsername(),
                'id'=>$booking->id
            ]);
        });

        $this->on('edit-booking-error', function ($message) {
            $this->log->error('Edit booking by {userid} failed {message}', [
                'userid'=>$this->user->getUsername(),
                'message'=>$message
            ]);
        });
    }

    public function getBookingUrl()
    {
        $settings = $this->settings;
        $scheme = $settings['scheme'];
        $port = $settings['port'];
        $uri = $this->request->getUri();

        $path = $this->router->urlFor('home');
        return $uri->withPath($path)->withPort($port)->withScheme($scheme);
    }
}
