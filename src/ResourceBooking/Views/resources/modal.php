<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<div class="modal fade" id="resourceModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="infoModalLabel">Resource Details</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm">
                    <tr>
                        <th>Resource</th>
                        <td><?=$this->e($resource->resource); ?></td>
                    </tr>
                    <tr>
                        <th>Group</th>
                        <td><?=$this->e($resource->group->resource_group); ?></td>
                    </tr>
                    <tr>
                        <th>Model</th>
                        <td><?=$this->e($resource->model); ?></td>
                    </tr>
                    <tr>
                        <th>Location</th>
                        <td><?=$this->e($resource->location); ?></td>
                    </tr>
                    <tr>
                        <th>Serial Number</th>
                        <td><?=$this->e($resource->serial_number); ?></td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td><?=nl2br($this->e($resource->description)); ?></td>
                    </tr>
                    <?php if ($resource->current_mileage) : ?>
                    <tr>
                        <th>Current Mileage</th>
                        <td><?=$this->e($resource->current_mileage); ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if ($resource->service_mileage) : ?>
                    <tr>
                        <th>Service Mileage</th>
                        <td><?=$this->e($resource->service_mileage); ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if ($resource->service_date) : ?>
                    <tr>
                        <th>Service Date</th>
                        <td><?=$this->e($resource->service_date); ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <th>Available</th>
                        <td><?=$this->yesNo($resource->available); ?></td>
                    </tr>
                </table>
                <div class="modal-footer">
                    <?php if ($acl->isAllowed($user, $resource, 'edit')) : ?>
                    <a href="<?=$router->urlFor('resource-edit', ['id'=>$resource->getPrimaryVal()]); ?>" class="btn btn-primary">Edit</a>
                    <?php endif; ?>
                    <?php if ($acl->isAllowed($user, $resource, 'delete')) : ?>
                    <a href="<?=$router->urlFor('resource-delete', ['id'=>$resource->getPrimaryVal()]); ?>" class="btn btn-danger">Delete</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>