<?php


use Phinx\Seed\AbstractSeed;

class BookingSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $handle = fopen(dirname(__DIR__) . "/seeds/resource_bookings.csv", "r");

        /**
         * NEED TO REFERENCE LDAP API for USERS! else foreign key will fail on userid
         * Users need to be added first
         */

        $rows = [];
        if ($handle !== false) {
            while (($row = fgetcsv($handle)) !== false) {
                $rows[] = [
                    'id'=>$row[0],
                    'title'=>$row[1],
                    'date_start'=>$row[2],
                    'date_end'=>$row[3],
                    'userid'=>$row[4],
                    'returned'=>$row[5],
                    'returned_by'=>$row[6] == 'NULL' ? null : $row['6'],
                    'returned_date'=>$row[7] == 'NULL' ? null : $row['7'],
                    'created_by'=>$row[8],
                    'updated_by'=>$row[9],
                ];
            }
        }

        fclose($handle);

        $booking = $this->table('booking');
        $booking->insert($rows)
              ->save();
    }

    public function getDependencies()
    {
        return [
            'UserSeeder',
        ];
    }
}
