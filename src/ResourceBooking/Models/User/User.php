<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace ResourceBooking\Models\User;

use Atlas\Mapper\Mapper;
use Atlas\Table\Row;

/**
 * @method UserTable getTable()
 * @method UserRelationships getRelationships()
 * @method UserRecord|null fetchRecord($primaryVal, array $with = [])
 * @method UserRecord|null fetchRecordBy(array $whereEquals, array $with = [])
 * @method UserRecord[] fetchRecords(array $primaryVals, array $with = [])
 * @method UserRecord[] fetchRecordsBy(array $whereEquals, array $with = [])
 * @method UserRecordSet fetchRecordSet(array $primaryVals, array $with = [])
 * @method UserRecordSet fetchRecordSetBy(array $whereEquals, array $with = [])
 * @method UserSelect select(array $whereEquals = [])
 * @method UserRecord newRecord(array $fields = [])
 * @method UserRecord[] newRecords(array $fieldSets)
 * @method UserRecordSet newRecordSet(array $records = [])
 * @method UserRecord turnRowIntoRecord(Row $row, array $with = [])
 * @method UserRecord[] turnRowsIntoRecords(array $rows, array $with = [])
 */
class User extends Mapper
{
}
