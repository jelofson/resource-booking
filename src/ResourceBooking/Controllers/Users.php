<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ResourceBooking\Controllers;

use \ResourceBooking\Models\User\User;

class Users extends Bread
{
    protected $mapper_class = User::class;

    public function index()
    {
        $content = $this->view->render('users/index', [
            'messages'=>$this->messages
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function search()
    {
        $name = filter_input(INPUT_GET, 'name', FILTER_SANITIZE_STRING);

        if (! $name) {
            return $this->badRequest('Missing required name parameter (?name=xxx)');
            $data = [
                'success'=>false,
                'status'=>500,
                'message'=>'Missing required name parameter (?name=xxx)'
            ];
            $this->response->getBody()->write(json_encode($data, JSON_PRETTY_PRINT));
            return $this->response->withHeader('Content-type', 'application/json')->withStatus(500);
        }

        $users = $this->atlas->select($this->mapper_class)
            ->columns('id', 'first_name', 'last_name', 'userid', 'email', 'role', 'active')
            ->where('last_name like ', $name . '%')
            ->fetchAll();

        // Map user rows into json with value, text as keys

        $mapped = [];
        foreach ($users as $user) {
            $mapped[] = [
                'value'=>$user['userid'],
                'text'=>$user['last_name'] . ', ' . $user['first_name']
            ];
        }

        $this->response->getBody()->write(json_encode($mapped, JSON_PRETTY_PRINT));
        return $this->response->withHeader('Content-type', 'application/json');

    }
}
