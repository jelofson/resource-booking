<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<?php $this->layout('layouts::' . $theme, ['title'=>$title]); ?>

<?=$this->alerts($messages); ?>

<h2>Delete Record</h2>

<h3>Are you sure you want to delete this booking?</h3>


<?=$form->begin(); ?>
<?=$form->getElement('csrf_token'); ?>

<?=$form->submit('Delete')->class('btn btn-danger'); ?>
<a href="<?=$cancel_href; ?>" class="btn btn-secondary">Cancel</a>
<?=$form->end(); ?>



