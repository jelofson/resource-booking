<?php
namespace Vespula\SlimMvc\Cli;

class Commands {
    public static function postCreateProject()
    {
        copy('./html/index.dist.php', './html/index.php'); 
        copy('./html/htaccess.dist', './html/.htaccess');
        mkdir('./html/app');
        unlink('README.md');
    }
}