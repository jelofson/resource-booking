<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ChangeUseridColumns extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        
        if ($this->hasTable('booking')) {
            $table = $this->table('booking');
            if ($table->hasForeignKey('userid')) {
                try {
                    $table->dropForeignKey('userid')->update();
                } catch (\Exception $e) {
                    echo 'Failed to remove the foreign key on the `userid` column. Please do so manually, then run the migration again' . PHP_EOL;
                    exit;
                }
                
            }
            if ($table->hasForeignKey('returned_by')) {
                try {
                    $table->dropForeignKey('returned_by')->update();
                } catch (\Exception $e) {
                    echo 'Failed to remove the foreign key on the `returned_by` column. Please do so manually, then run the migration again' . PHP_EOL;
                    exit;
                }
            }

            if ($table->hasForeignKey('created_by')) {
                try {
                    $table->dropForeignKey('created_by')->update();
                } catch (\Exception $e) {
                    echo 'Failed to remove the foreign key on the `created_by` column. Please do so manually, then run the migration again' . PHP_EOL;
                    exit;
                }
            }

            if ($table->hasForeignKey('updated_by')) {
                try {
                    $table->dropForeignKey('updated_by')->update();
                } catch (\Exception $e) {
                    echo 'Failed to remove the foreign key on the `updated_by` column. Please do so manually, then run the migration again' . PHP_EOL;
                    exit;
                }
            }
            
            $table->changeColumn('userid', 'string', ['limit'=>12])
                  ->changeColumn('returned_by', 'string', ['limit'=>12])
                  ->changeColumn('created_by', 'string', ['limit'=>12])
                  ->changeColumn('updated_by', 'string', ['limit'=>12])
                  ->update();

            $table->addForeignKey('userid', 'employees.employee', 'userid')
                  ->addForeignKey('returned_by', 'employees.employee', 'userid')
                  ->addForeignKey('created_by', 'employees.employee', 'userid')
                  ->addForeignKey('updated_by', 'employees.employee', 'userid')
                  ->update();

        }
    }
}
