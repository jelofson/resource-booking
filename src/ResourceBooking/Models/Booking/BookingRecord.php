<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace ResourceBooking\Models\Booking;

use ResourceBooking\Models\Record;

/**
 * @method BookingRow getRow()
 */
class BookingRecord extends Record
{
    use BookingFields;

    public $overdue = false;

    public function validate(): void
    {
        $required = [
            'userid'=>'Userid',
            'title'=>'Title',
            'date_start'=>'Start date',
            'date_end'=>'End date',
            'returned'=>'Returned',
            'created_by'=>'Created by',
            'updated_by'=>'Updated by'
        ];

        foreach ($required as $field=>$message) {
            if (is_string($this->$field)) {
                $this->$field = trim($this->$field);
            }  
            if ($this->$field === '' || $this->$field === null) {
                $this->addError("$message is required " . $this->$field);
            }
        }

        $start = new \DateTime($this->date_start);
        $end = new \DateTime($this->date_end);
        $end->sub(new \DateInterval('P1D'));

        
        if ($end < $start) {
            $this->addError('End date is before start date');
        }
    }

    public function isOverdue()
    {
        if ($this->returned == 1) {
            return false;
        }

        $today = new \DateTime();
        $end = new \DateTime($this->date_end);
        if ($end <= $today) {
            return true;
        }
        return false;
    }

    public function getResourcesAsArray()
    {
        if (! $this->bookingResources) {
            return [];
        }

        $resources = [];

        foreach ($this->bookingResources as $br) {
            if ($br->resource) {
                $resources[] = $br->resource->getArrayCopy();
            }
        }

        return $resources;
    }
    public function getResourcesAsString($separator = '; ', $prefix = '')
    {
        if (! $this->bookingResources) {
            return '';
        }

        $resources = [];

        foreach ($this->bookingResources as $br) {
            if ($br->resource) {
                $resources[] = $prefix . $br->resource->resource;
            }
        }

        return implode($separator, $resources);
    }
}
