<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ResourceBooking\Models;

use Atlas\Mapper\Record as AtlasRecord;
use Laminas\Permissions\Acl\Resource\ResourceInterface;
use ResourceBooking\Controllers\Bread;

abstract class Record extends AtlasRecord implements ResourceInterface
{   
    protected $updated_col = 'updated';
    protected $created_col = 'created';
    protected $created_by_col = 'created_by';
    protected $updated_by_col = 'updated_by';
    protected $errors = [];
    
    /**
     * Magic method to get a property that has a language component (title_en, title_fr)
     * For example $record->title('en') would return $record->title_en;
     * 
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if ($arguments) {
            $lang = $arguments[0];
        }

        $property = $name . '_' . $lang;
        
        return $this->$property;
        
    }

    /**
     * Get the Laminas ACL resource (lowercase of the record class)
     * 
     * @return string
     */
    public function getResourceId() :string
    {
        $class = get_class($this);
        $pos = strrpos($class,  '\\');
        $record_class = substr($class, $pos + 1);
        $resource_id = strtolower(str_replace('Record', '', $record_class));
        return $resource_id;
    }

    /**
     * Try to get the value of the primary key (assuming a single column key)
     * 
     * @return mixed
     */
    public function getPrimaryVal()
    {
        $table_class = $this->getMapperClass() . 'Table';

        $primary_keys = $table_class::PRIMARY_KEY;

        // Should throw exception if more than one column?

        $primary_key = $primary_keys[0];

        return $this->$primary_key;
        
    }

    /**
     * Get the name of the primary key column, assuming only one column
     * 
     * @return string
     */
    public function getPrimaryCol() :string
    {
        $table_class = $this->getMapperClass() . 'Table';

        $primary_keys = $table_class::PRIMARY_KEY;
        $primary_key = $primary_keys[0];

        return $primary_key;
        
    }

    /**
     * Attempt to set the created, updated, created_by, updated_by values.
     * 
     * @return void
     */
    public function setCreatedUpdated($action, $user, $timestamp_format = 'Y-m-d H:i:s') :void
    {
        $now = date($timestamp_format);

        $updated_col = $this->updated_col;
        $updated_by_col = $this->updated_by_col;
        
        if ($this->has('updated')) {
            $this->$updated_col = $now;
        }

        if ($this->has('updated_by')) {
            $this->$updated_by_col = $user->getUsername();
        }

        if ($action == Bread::ACTION_ADD) {
            $created_col = $this->created_col;
            $created_by_col = $this->created_by_col;

            if ($this->has('created')) {
                $this->$created_col = $now;
            }
    
            if ($this->has('created_by')) {
                $this->$created_by_col = $user->getUsername();
            }
        }
    }

    public function getErrors() :array
    {
        return $this->errors;
    }

    public function addError($message) :void
    {
        $this->errors[] = $message;
    }

    public function isValid() :bool
    {
        return count($this->errors) == 0;
    }

    public function validate() :void
    {
    }
}