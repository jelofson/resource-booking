<?php


use Phinx\Seed\AbstractSeed;

class ResourceGroupSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'id'=>1,
                'resource_group'=>'Informatics',
                'userid'=>'rpolishw'
            ],
            [
                'id'=>2,
                'resource_group'=>'Quads/Trailers',
                'userid'=>'btomm,tcail'
            ],
            [
                'id'=>3,
                'resource_group'=>'RSGIS Group Shared Equipment',
                'userid'=>'mgartrel'
            ],
            [
                'id'=>4,
                'resource_group'=>'Field Safety',
                'userid'=>'tcail'
            ],
            [
                'id'=>5,
                'resource_group'=>'Carpentry Shop',
                'userid'=>'cmcnalty'
            ],
        ];

        $resource_group = $this->table('resource_group');
        $resource_group->insert($data)
                ->save();
    }
    
}
