<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

final class BookingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        if ( ! $this->hasTable('booking')) {
            $table = $this->table('booking');
            $table->addColumn('title', 'string', ['limit'=>100])
                  ->addColumn('date_start', 'date')
                  ->addColumn('date_end', 'date')
                  ->addColumn('userid', 'string', ['limit'=>25])
                  ->addColumn('returned', 'integer', ['limit'=>MysqlAdapter::INT_TINY, 'signed'=>false, 'default'=>0])
                  ->addColumn('returned_by', 'string', ['limit'=>8, 'null'=>true])
                  ->addColumn('returned_date', 'date', ['limit'=>150, 'null'=>true])
                  ->addColumn('created_by', 'string', ['limit'=>8])
                  ->addColumn('updated_by', 'string', ['limit'=>8])
                  ->addIndex('userid')
                  ->create();
            $table->addForeignKey('userid', 'user', 'userid')->update();
        }
    }
}
