<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ResourceBooking\Controllers;



class Index extends Controller
{

    public function login()
    {
        $this->form->autoLf();

        $settings = $this->settings;
        $network_policy_string = $settings['network_policy_string'] ?? '';
        $network_policy_url = $settings['network_policy_url'] ?? '';

        $csrf_token = $this->session->getCsrfToken()->getValue();
        $content = $this->view->render('index/login', [
            'messages'=>$this->messages,
            'form'=>$this->form,
            'csrf_token'=>$csrf_token, 
            'network_policy_string'=>$network_policy_string,
            'network_policy_url'=>$network_policy_url,
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function doLogin()
    {
        $referer = $this->request->getAttribute('referer');
        if (! $referer) {
            $referer = $this->router->urlFor('home');
        }
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_DEFAULT);
        $csrf_token_value = filter_input(INPUT_POST, 'csrf_token', FILTER_SANITIZE_STRING);

        $csrf_token = $this->session->getCsrfToken();
        
        if (! $csrf_token->isValid($csrf_token_value)) {
            throw new \Exception('Invalid token. Perhaps your session expired, or you are trying something your should not?');
        }

        $password = strip_tags($password);
        $credentials = [
            'username'=>$username,
            'password'=>$password
        ];
        

        $this->auth->login($credentials);
        if ($this->auth->isValid()) {
            $this->trigger('login', $username);
            return $this->redirect($referer);
        } else {
            $this->trigger('login-fail', $username);
            return $this->redirect($this->router->urlFor('login'));
        }
        
    }

    public function authCheck()
    {
        if ($this->auth->isValid()) {
            $content = [
                'valid'=>true
            ];
            $this->response->getBody()->write(json_encode($content));
            return $this->response->withHeader('Content-type', 'application/json');
        }

        $content = [
            'valid'=>false
        ];
        $this->response->getBody()->write(json_encode($content));
        return $this->response->withHeader('Content-type', 'application/json');
    }

    public function logout()
    {
        $referer = $this->request->getAttribute('referer');
        if (! $referer) {
            $referer = $this->router->urlFor('home');
        }
        
        if (! $this->auth->isValid()) {
            $this->setFlash('info', 'You are not logged in');
            return $this->redirect($referer);
        }


        
        $username = $this->auth->getUsername();
        $this->auth->logout();
        if ($this->auth->isAnon()) {
            $this->trigger('logout', $username);
            return $this->redirect($referer);
        } else {
            $this->trigger('logout-fail', $username);
            return $this->redirect($this->router->urlFor('home'));
        }
    }

    protected function registerEvents()
    {
        $this->on('login', function ($username) {
            $adapter = $this->auth->getAdapter();
            if (method_exists($adapter, 'syncDb')) {
                //$adapter->syncDb($this->auth->getUserdata());
                //$this->log->debug('Syncing DB with LDAP Data');

            }

            $this->setFlash('info', 'You are logged in');
            $this->log->info($username . ' logged in');

        });

        $this->on('login-fail', function ($username) {
            $this->setFlash('error', 'Bad login');
            $this->log->error($username . ' failed to log in');

        });

        $this->on('logout', function ($username) {
            $this->setFlash('info', 'Goodbye');
            $this->log->info($username . ' logged out');
        });

        $this->on('logout-fail', function ($username) {
            $this->setFlash('error', 'Could not log you out...');
            $this->log->error($username . ' failed logging out');
        });

    }
    
}
