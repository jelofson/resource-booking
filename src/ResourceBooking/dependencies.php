<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

use Vespula\PlatesExtensions\Alerts;
use Vespula\Log\Log as Log;
use Vespula\Log\Adapter\Sql as SqlLog;
use Vespula\Auth\Session\Session as AuthSession;
use Vespula\Auth\Auth;
use Vespula\Auth\Adapter\Text as AuthTextAdapter;
use Vespula\Auth\Adapter\Ldap as AuthLdapAdapter;
use Vespula\Auth\Adapter\Sql as AuthSqlAdapter;
use Aura\Session\SessionFactory;
use League\Plates\Engine as View;
use Vespula\Locale\Locale;
use Vespula\User\User;
use Vespula\Form\Form;
use Vespula\Form\Builder\Builder as FormBuilder;
use Laminas\Permissions\Acl\Acl;
use Vespula\Paginator\Decorator\Generic as PaginatorDecoratorGeneric;
use Vespula\Paginator\Paginator;
use Atlas\Orm\Atlas;

/**
 * @var \League\Container\Container $container
 */

$container->defaultToShared();

$container->add('settings', function () use ($settings) {
    return $settings;
});

//$container->add('logAdapter', ErrorLog::class)->addArgument(ErrorLog::TYPE_PHP_LOG);
$container->add('logAdapter', function () use ($container) {
    $pdo = $container->get('pdo');
    $sql_adapter = new SqlLog($pdo, 'log');
    return $sql_adapter;
});

$container->add('log', function () use ($container) {
    $adapter = $container->get('logAdapter');
    $logger = new Log('sql', $adapter);
    return $logger;
});

$container->add('session', function () {
    $sessionFactory = new SessionFactory();
    return $sessionFactory->newInstance($_COOKIE);
});

$container->add('view', function () use ($container) {

    $settings = $container->get('settings');
    $theme = $settings['theme'];
    $title = $settings['title'];
    $base_uri = $settings['base_uri'];
    $env = $settings['env'];

    $views = dirname(__FILE__) . '/Views';
    $layouts = dirname(__FILE__) . '/Layouts';

    $view = new View($views);
    $view->addFolder('layouts', $layouts);

    $view->addData([
        'theme'=>$theme,
        'title'=>$title,
        'base_uri'=>$base_uri,
        'auth'=>$container->get('auth'),
        'router'=>$container->get('router'),
        'env'=>$env,
        'acl'=>$container->get('acl'),
        'user'=>$container->get('user'),
    ]);

    $alerts = new Alerts($container);
    $alerts->setTheme('bootstrap');
    $view->loadExtension($alerts);

    $view->registerFunction('endDate', function ($dateString) {
        $endDate = new \DateTime($dateString);
        $endDate->sub(new \DateInterval('P1D'));
        return htmlentities($endDate->format('Y-m-d'), ENT_COMPAT, 'UTF-8');
    });

    $view->registerFunction('yesNo', function ($value) {
        switch ($value) {
            case 0 :
                $output = '<i class="fas fa-times"></i>';
            break;
            case 1 :
                $output = '<i class="fas fa-check"></i>';
            break;
            default :
                $output = '<i class="fas fa-question"></i>';
            break;
        }
        return $output;
    });

    return $view;

});

$container->add('locale', Locale::class)->addArgument('en_CA')->addMethodCall('load', [__DIR__ . '/Locales']);
$container->add('user', User::class)->addArgument('guest');
$container->add('authSession', AuthSession::class);
//$container->add('authSession', AuthSession::class)->addMethodCall('setIdle', [30]);

$container->add('authAdapterText', function () {
    $passwords = [
        'juser'=>'$2y$10$smIIzBtPIDz82ye/lTa5ruIqNIT7qFB3dbQr8ad9Xx9IRn9IF.l.C' // password is 'test'
    ];
    $adapter = new AuthTextAdapter($passwords);

    $adapter->setUserdata('juser', [
        'first_name'=>'Joe',
        'last_name'=>'User',
        'email'=>'joe.user@example.com',
    ]);
    return $adapter;
});

$container->add('authAdapterSql', function () use ($container) {
    
    $cols = [
        'first_name',
        'last_name',
        'email',
        'userid'=>'username',
        'password',
        'role',
        'active'
    ];
    $from = 'employee';
    $where = 'active = 1';
    $pdo = $container->get('pdo');
    $adapter = new AuthSqlAdapter($pdo, $from, $cols, $where);

    return $adapter;
});

$container->add('authAdapterLdap', function () use ($container) {

    $settings = $container->get('settings');
    
    $uri = $settings['ldap_uri'];
    //$bind_options = $settings['bindOptions'];
    $dn = $settings['ldap_dn'];
    $ldap_options = [
        LDAP_OPT_PROTOCOL_VERSION=>3,
        LDAP_OPT_REFERRALS=>0
    ];

    $attributes = [
        'email' => 'mail',
        'first_name' => 'givenname',
        'last_name' => 'sn',
        'username'=>'cn'
    ];

    //$adapter = new AuthLdapAdapter($uri, $dn, null, $ldap_options, $attributes);
    $adapter = new \ResourceBooking\Auth\Adapter\Ldap($uri, $dn, null, $ldap_options, $attributes);
    $adapter->setAtlas($container->get('atlas'));
    return $adapter;
});

$authAdapter = $settings['authAdapter'] ?? 'authAdapterText';

$container->add('auth', Auth::class)->addArgument($authAdapter)->addArgument('authSession');
$pdo_settings = $settings['atlas']['pdo'];
$container->add('pdo', \PDO::class)->addArguments([
    $pdo_settings[0],
    $pdo_settings[1],
    $pdo_settings[2],
]);

$container->add('formBuilder', FormBuilder::class)
    ->addArgument('pdo')
    ->addMethodCall('setDefaultClasses', [
        $settings['form']['default_classes']
    ])
    ->addMethodCall('setBlacklist', [
        $settings['form']['blacklist']
    ]
);

$container->add('form', Form::class)
    ->addMethodCall('setBuilder', ['formBuilder'])
    ->addMethodCall('autoLf');

$container->add('acl', function () use ($container) {
    $acl = new Acl();
    // See the separate acl.php file for additional rules and resources.

    return $acl;
});

$container->add('paginatorDecorator', PaginatorDecoratorGeneric::class);
$container->add('paginator', Paginator::class)->addArgument('paginatorDecorator');


$container->add('atlas', function () use ($container) {
    $settings = $container->get('settings');
    $atlas = Atlas::new(
        $settings['atlas']['pdo'][0],
        $settings['atlas']['pdo'][1],
        $settings['atlas']['pdo'][2]
    );
    return $atlas;
});

$container->add('select', function () use ($container) {
    $pdo = $container->get('pdo');
    $select = \Atlas\Query\Select::new($pdo);
    return $select;
});

$container->add('mailMessage', function () {
    $message = new \Laminas\Mail\Message();
    $message->setEncoding('utf-8');
    return $message;
});

$container->add('mailTransportSmtp', function () use ($container) {

    $settings = $container->get('settings');

    $transport = new \Laminas\Mail\Transport\Smtp();
    
    return $transport;
    
});

$container->add('mailTransportFile', function () use ($container) {

    $settings = $container->get('settings');

    $save_path = $settings['email_save_path'];
    $transport = new \Laminas\Mail\Transport\File();
    $options = new Laminas\Mail\Transport\FileOptions([
        'path'=>$save_path
    ]);
    
    $transport->setOptions($options);
    
    return $transport;
    
});

$container->add('mailTransportSendmail', function () use ($container) {

    $transport = new \Laminas\Mail\Transport\Sendmail();
    return $transport;
    
});

$container->add('mailTransport', function () use ($container) {

    $settings = $container->get('settings');

    $transport = $settings['mailTransport'] ?? 'mailTransportFile';
    
    return $container->get($transport);
    
});
