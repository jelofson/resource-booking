<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<?php $this->layout('layouts::' . $theme); ?>

<h1>Resources available to book</h1>

<?=$this->alerts($messages); ?>
<?php if ($acl->isAllowed($user, 'resource', 'add')) : ?>
<p>[<a href="<?=$router->urlFor('resource-add'); ?>">Add new</a>]</p>
<?php endif; ?>

<?php if ($records) : ?>
<table class="table table-sm table-striped">
    <thead>
        <tr>
            <th>Group</th>
            <th>Resource</th>
            <th>Model</th>
            <th>Location</th>
            <th>Available</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($records as $record) : ?>
        <tr>
            <td><?=$this->e($record->group->resource_group); ?></td>
            <td><a class="resource" href="<?=$router->urlFor('resource-read', ['id'=>$record->getPrimaryVal()]); ?>"><?=$this->e($record->resource); ?></a></td>
            <td><?=$this->e($record->model); ?></td>
            <td><?=$this->e($record->location); ?></td>
            <td><?=$this->yesNo($record->available); ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php else : ?>
<p>No resources.</p>
<?php endif; ?>


<div id="infoModal"></div>
<?php $this->start('scripts'); ?>

<script>
$(function () {
    $("a.resource").on('click', function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        $.get(href, function (data) {
            $("#infoModal").html(data);
            $("#resourceModal").modal('show');
        }).fail(function (data) {
            alert('Error! ' + data.message);
        });
    });
});
</script>
<?php $this->end(); ?>