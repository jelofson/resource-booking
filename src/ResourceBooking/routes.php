<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use ResourceBooking\Controllers\Index;
use ResourceBooking\Controllers\Bookings;
use ResourceBooking\Controllers\Resources;
use ResourceBooking\Controllers\Users;


$app->get('/', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->index();
})->setName('home');

$app->get('/bookings', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->bookings();
})->setName('bookings');

$app->get('/booking', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->booking();
})->setName('booking');

$app->get('/return/{id}', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->return($args['id']);
})->setName('return');

$app->get('/unreturn/{id}', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->unreturn($args['id']);
})->setName('unreturn');

$app->get('/bookings/add', function (Request $request, Response $response) {
    $controller = new Bookings($this, $request, $response);
    return $controller->add();
})->setName('add-booking');

$app->post('/bookings/add', function (Request $request, Response $response) {
    $controller = new Bookings($this, $request, $response);
    return $controller->doAdd();
});

$app->get('/bookings/edit/{id}', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->edit($args['id']);
})->setName('edit-booking');

$app->post('/bookings/edit/{id}', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->doEdit($args['id']);
});

$app->get('/bookings/delete/{id}', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->delete($args['id']);
})->setName('delete');

$app->post('/bookings/delete/{id}', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->doDelete($args['id']);
});

$app->get('/resource-select', function (Request $request, Response $response) {
    $controller = new Bookings($this, $request, $response);
    return $controller->resourceSelect();
})->setName('resource-select');


$app->get('/resources', function (Request $request, Response $response) {
    $controller = new Resources($this, $request, $response);
    return $controller->browse();
})->setName('resource-browse');

$app->get('/resources/add', function (Request $request, Response $response) {
    $controller = new Resources($this, $request, $response);
    return $controller->add();
})->setName('resource-add');

$app->post('/resources/add', function (Request $request, Response $response) {
    $controller = new Resources($this, $request, $response);
    return $controller->doAdd();
});

$app->get('/resources/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $controller = new Resources($this, $request, $response);
    return $controller->read($args['id']);
})->setName('resource-read');

$app->get('/resources/edit/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $controller = new Resources($this, $request, $response);
    return $controller->edit($args['id']);
})->setName('resource-edit');

$app->post('/resources/edit/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $controller = new Resources($this, $request, $response);
    return $controller->doEdit($args['id']);
});


$app->get('/resources/delete/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $controller = new Resources($this, $request, $response);
    return $controller->delete($args['id']);
})->setName('resource-delete');

$app->post('/resources/delete/{id:[0-9]+}', function (Request $request, Response $response, $args) {
    $controller = new Resources($this, $request, $response);
    return $controller->doDelete($args['id']);
});

$app->get('/resources/json', function (Request $request, Response $response) {
    $controller = new Resources($this, $request, $response);
    return $controller->json();
})->setName('resource-json');

$app->get('/bookings/overdue', function (Request $request, Response $response, $args) {
    $controller = new Bookings($this, $request, $response);
    return $controller->checkOverdue();
})->setName('booking-overdue');

// Login Route for ResourceBooking\Index
$app->get('/login', function (Request $request, Response $response, $args) {
    $controller = new Index($this, $request, $response);
    return $controller->login();
})->setName('login');


// Login Route (POST) for ResourceBooking\Index
$app->post('/login', function (Request $request, Response $response, $args) {
    $controller = new Index($this, $request, $response);
    return $controller->doLogin();
});


// Logout Route for ResourceBooking\Index
$app->get('/logout', function (Request $request, Response $response, $args) {
    $controller = new Index($this, $request, $response);
    return $controller->logout();
})->setName('logout');

$app->get('/auth-check', function (Request $request, Response $response, $args) {
    $controller = new Index($this, $request, $response);
    return $controller->authCheck();
})->setName('auth-check');

// Bread routes for ResourceBooking\Users
$app->get('/users', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->browse();
})->setName('user-browse');

$app->get('/users/read/{id}', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->read($args['id']);
})->setName('user-read');

$app->get('/users/edit/{id}', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->edit($args['id']);
})->setName('user-edit');

$app->post('/users/edit/{id}', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->doEdit($args['id']);
});

$app->get('/users/add', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->add();
})->setName('user-add');

$app->post('/users/add', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->doAdd();
});

$app->get('/users/delete/{id}', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->delete($args['id']);
})->setName('user-delete');

$app->post('/users/delete/{id}', function (Request $request, Response $response, $args) {
    $controller = new \ResourceBooking\Controllers\Users($this, $request, $response);
    return $controller->doDelete($args['id']);
});

$app->get('/users/search', function (Request $request, Response $response) {
    $controller = new Users($this, $request, $response);
    return $controller->search();
})->setName('user-search');

