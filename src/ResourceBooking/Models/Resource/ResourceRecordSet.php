<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

 
declare(strict_types=1);

namespace ResourceBooking\Models\Resource;

use Atlas\Mapper\RecordSet;

/**
 * @method ResourceRecord offsetGet($offset)
 * @method ResourceRecord appendNew(array $fields = [])
 * @method ResourceRecord|null getOneBy(array $whereEquals)
 * @method ResourceRecordSet getAllBy(array $whereEquals)
 * @method ResourceRecord|null detachOneBy(array $whereEquals)
 * @method ResourceRecordSet detachAllBy(array $whereEquals)
 * @method ResourceRecordSet detachAll()
 * @method ResourceRecordSet detachDeleted()
 */
class ResourceRecordSet extends RecordSet
{
    public function asString($separator = '; ', $prefix = '')
    {
        $strings = [];
        foreach ($this->getRecords() as $record) {
            $strings[] = $prefix . $record->resource;
        }

        return implode($separator, $strings);
    }
}
