<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<div class="modal fade" id="bookingModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?=$booking->title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    <strong>Start Date:</strong> <?=$this->e($booking->date_start); ?><br/>
                    <strong>End Date:</strong> <?=$this->endDate($booking->date_end); ?><br/>
                    <strong>Reserved By:</strong> <?=$this->e($booking->user->getFullName()); ?>
                </p>
                <p><strong>Resources:</strong></p>
                <ul>
                    <?php foreach ($booking->bookingResources as $br) : ?>
                    <li><?=$this->e($br->resource->resource); ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php if ($booking->returned == "1") : ?>
                <p>
                    These resources were returned by <?=$this->e($booking->returned_by); ?> on <?=$this->e($booking->returned_date); ?>. 
                    Once resources are returned, you cannot delete or modify the booking. Ask your administrator if required.
                </p>
                <?php endif; ?>
                <?php if ($booking->isOverdue()) : ?>
                <p class="bg-warning px-2"><strong>These resources are due or past due.</strong></p>
                <?php endif; ?>
            </div>
            <div class="modal-footer">
            <?php if ($booking->returned == "0") : ?>
                <?php if ($acl->isAllowed($user, $booking, 'return')) : ?>
                <a href="<?=$router->urlFor('return', ['id'=>$booking->id]); ?>" class="btn btn-primary" id="btn-return">Mark Returned</a>
                <?php endif; ?>
                <?php if ($acl->isAllowed($user, $booking, 'edit')) : ?>
                <a href="<?=$router->urlFor('edit-booking', ['id'=>$booking->id]); ?>" id="editButton" class="btn btn-secondary">Edit</a>
                <?php endif; ?>
                <?php if ($acl->isAllowed($user, $booking, 'delete')) : ?>
                <a href="<?=$router->urlFor('delete', ['id'=>$booking->id]); ?>" class="btn btn-danger">Delete</a>
                <?php endif; ?>
            <?php else : ?>
                <?php if ($acl->isAllowed($user, $booking, 'unreturn')) : ?>
                <a href="<?=$router->urlFor('unreturn', ['id'=>$booking->id]); ?>" class="btn btn-primary" id="btn-unreturn">Mark Not Returned</a>
                <?php endif; ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
$(function () {
    
    $("#btn-return").on('click', function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        Swal.fire({
            icon: 'question',
            text: 'Are you sure you want to mark these resources as returned?',

            showCancelButton: true,
            confirmButtonText: `Yes, please`,
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = href;
            }
        })
    });
    $("#btn-unreturn").on('click', function (e) {
        e.preventDefault();
        let href = $(this).attr('href');
        Swal.fire({
            icon: 'question',
            text: 'Are you sure you want to mark these resources as NOT returned?',

            showCancelButton: true,
            confirmButtonText: `Yes, please`,
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = href;
            }
        })
    });
});
</script>