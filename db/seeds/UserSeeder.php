<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        // Seed everyone in the bookings csv file

        $handle = fopen(dirname(__DIR__) . "/seeds/resource_bookings.csv", "r");

        $settings = include dirname(dirname(__DIR__)) . "/src/ResourceBooking/settings.php";
        $employee_api_url = $settings['employee_api_urls']['record'] ?? null;
        if (! $employee_api_url) {
            throw new \Exception('Missing employee api url');
        }
        /**
         * NEED TO REFERENCE LDAP API for USERS! else foreign key will fail on userid
         * Users need to be added first
         */

        $user_rows = [];

        // Add a few known ones for the resource groups
        $users = $settings['user_seeder_users'] ?? [];
        $admin_user = $settings['user_seeder_admin'] ?? null;

        if ($handle !== false) {
            while (($row = fgetcsv($handle)) !== false) {
                $users[] = $row[4];
                if (isset($row[6])) {
                    $users[] = $row[6];
                }
                if (isset($row[8])) {
                    $users[] = $row[8];
                }
                if (isset($row[9])) {
                    $users[] = $row[9];
                }
            }
        }

        fclose($handle);

        $users = array_unique($users);
        foreach ($users as $userid) {
            $url = sprintf($employee_api_url, $userid);
        
            $ch = curl_init($url);
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            
            $curl_info = curl_getinfo($ch);
            $status_code = $curl_info['http_code'] ?? null;
            curl_close($ch);

            if ($status_code != 200) {
                echo "Could not find user in AD " . $userid . PHP_EOL;
                continue;
            }
            if ($output === false) {
                echo "Could not find user in AD " . $userid . PHP_EOL;
                continue;
            }

            $json = json_decode($output);
            $user_data = $json->data;

            $role = $userid == $admin_user ? 'admin' : 'user';
            
            $user_rows[] = [
                'id'=>null,
                'first_name'=>$user_data->givenname,
                'last_name'=>$user_data->lastname,
                'email'=>$user_data->email,
                'userid'=>$userid,
                'password'=>password_hash($userid, PASSWORD_DEFAULT),
                'role'=>$role,
                'active'=>1
            ];
        }

        $user = $this->table('user');
        $user->insert($user_rows)
              ->save();
        
    }
}
