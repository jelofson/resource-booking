<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class EmployeeView extends AbstractMigration
{
    
    public function up(): void
    {
        $this->execute('CREATE OR REPLACE VIEW `employee` AS select * from employees.employee order by last_name;');
    }

    public function down(): void
    {
        $this->execute('DELETE VIEW `employee`;');
    }
}
