<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ResourceBooking\Controllers;

use ResourceBooking\Models\Resource\Resource;
use ResourceBooking\Models\ResourceGroup\ResourceGroup;

class Resources extends Bread
{
    protected $mapper_class = Resource::class;
    protected $browse_template = 'resources/browse';

    public function index()
    {
        $content = $this->view->render('resources/index', [
            'messages'=>$this->messages
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function browse()
    {
        if (! $this->acl->isAllowed($this->user, 'resource', 'browse')) {
            return $this->notAllowed();
        }

        $resources = $this->atlas->select(Resource::class)
            ->with(['group'])
            ->orderBy('resource_group_id', 'resource ASC')
            ->fetchRecordSet();

        $content = $this->view->render('resources/browse', [
            'messages'=>$this->messages,
            'records'=>$resources
        ]);

        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function json()
    {
        if (! $this->acl->isAllowed($this->user, 'resource', 'json')) {
            return $this->notAllowed();
        }

        $resources = $this->atlas->select(Resource::class)
            ->with(['group'])
            ->orderBy('resource_group_id', 'resource ASC')
            ->where('available = 1')
            ->fetchRecordSet();

        $array = [];

        $colors = [
            1=>'green',
            2=>'orange',
            3=>'blue',
            4=>'brown',
            5=>'DarkSlateGrey'
        ];

        foreach ($resources as $resource) {
            $color = $colors[$resource->resource_group_id] ?? 'blue';
            $array[] = [
                'id'=>$resource->id,
                'group'=>$resource->group->resource_group,
                'title'=>$resource->resource,
                'evenColor'=>$color
            ];
        }
        

        $this->response->getBody()->write(json_encode($array, JSON_PRETTY_PRINT));
        return $this->response->withHeader('Content-type', 'application/json');
    }

    public function read($id)
    {
        $resource = $this->atlas->fetchRecord(Resource::class, $id, ['group']);
        if (! $resource) {
            return $this->notFound();
        }
        $content = $this->view->render('resources/modal', [
            'resource'=>$resource
        ]);
        $this->response->getBody()->write($content);
        return $this->response;
    }

    public function delete($id)
    {
        $this->cancel_href = $this->router->urlFor('resource-browse');
        return parent::delete($id);
    }

    protected function beforeBuildForm(): void
    {
        $groups = $this->atlas->select(ResourceGroup::class)
            ->orderBy('resource_group ASC')
            ->columns('id', 'resource_group')
            ->fetchKeyPair();
        
        $groupOpts = [''=>'-------'] + $groups;

        $builder = $this->form->getBuilder();

        $builder->setColumnOptions([
            'resource_group_id'=>[
                'type'=>'select',
                'callback'=>function ($element) use ($groupOpts) {
                    $element->options($groupOpts);
                }
            ],
            'available'=>[
                'type'=>'select',
                'callback'=>function ($element) {
                    $opts = [
                        '0'=>'Not Available',
                        '1'=>'Available'
                    ];
                    $element->options($opts);
                }
            ]
        ]);

        $this->cancel_href = $this->router->urlFor('resource-browse');
    }

    protected function beforeSave()
    {
        $this->post_save_redirect = $this->router->urlFor('resource-browse');
    }

    protected function sanitizePost()
    {
        $fields = array_keys($this->request->getParsedBody());

        $nullable = [
            'service_date',
            'model',
            'serial_number',
            'description',
            'image',
            'current_mileage',
        ];

        $clean = [];



        foreach ($fields as $field) {
            $filtered = filter_input(INPUT_POST, $field, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES|FILTER_NULL_ON_FAILURE);
            if ($field == "available") {
                $filtered = (int) $filtered;
            }
            if ($filtered === "" && in_array($field, $nullable)) {
                $filtered = null;
            }
            $clean[$field] = $filtered;
        }

        return $clean;
    }
}
