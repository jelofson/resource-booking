<?php

namespace ResourceBooking\Error\Renderers;

use Throwable;
use League\Plates\Engine as ViewEngine;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpException;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpMethodNotAllowedException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpNotImplementedException;
use Slim\Exception\HttpUnauthorizedException;

class HtmlErrorRenderer 
{
    public const BAD_REQUEST = 'BAD_REQUEST';
    public const METHOD_NOT_ALLOWED = 'METHOD_NOT_ALLOWED';
    public const NOT_IMPLEMENTED = 'NOT_IMPLEMENTED';
    public const RESOURCE_NOT_FOUND = 'RESOURCE_NOT_FOUND';
    public const SERVER_ERROR = 'SERVER_ERROR';
    public const UNAUTHORIZED = 'UNAUTHORIZED';
    public const FORBIDDEN = 'FORBIDDEN';

    protected $view;

    protected $type_strings = [
        'RESOURCE_NOT_FOUND'=>'Not found',
        'BAD_REQUEST'=>'Bad Request',
        'METHOD_NOT_ALLOWED'=>'Method Not Allowed',
        'FORBIDDEN'=>'Forbidden',
        'UNAUTHORIZED'=>'Unauthorized',
        'SERVER_ERROR'=>'Server Error',
        'NOT_IMPLEMENTED'=>'Not Implemented',
    ];


    public function __construct(ViewEngine $view)
    {
        $this->view = $view;
    }

    public function setTypeStrings(array $type_strings)
    {
        $this->type_strings = $type_strings;
    }

    public function __invoke(Throwable $exception, bool $displayErrorDetails): string
    {
        $statusCode = 500;
        $type = self::SERVER_ERROR;
        $description = 'An internal error has occurred while processing your request.';

        $exceptionClass = get_class($exception);

        if ($exception instanceof HttpException) {
            $statusCode = $exception->getCode();
            $description = $exception->getMessage();
            $exceptionClass = get_class($exception);

            switch ($exceptionClass) {
                case HttpNotFoundException::class :
                    $type = self::RESOURCE_NOT_FOUND;
                break;
                case HttpMethodNotAllowedException::class :
                    $type = self::METHOD_NOT_ALLOWED;
                break;
                case HttpUnauthorizedException::class :
                    $type = self::UNAUTHORIZED;
                break;
                case HttpForbiddenException::class :
                    $type = self::FORBIDDEN;
                break;
                case HttpBadRequestException::class :
                    $type = self::BAD_REQUEST;
                break;
                case HttpNotImplementedException::class :
                    $type = self::NOT_IMPLEMENTED;
                break;
            }
        }
        
        $content = $this->view->render('common/error', [
            'page_title'=>$statusCode . ' ' . $this->type_strings[$type] ?? $type,
            'status'=>$statusCode,
            'description'=>$description,
            'exception'=>$exception,
            'display_details'=>$displayErrorDetails
        ]);

        return $content;
    }
}