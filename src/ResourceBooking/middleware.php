<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use ResourceBooking\Models\User\User as UserMapper;

// Note that last middleware added is first to be executed

$userMiddleware = function (Request $request, RequestHandler $handler) {
    // $this refers to the container
    $auth = $this->get('auth');
    $user = $this->get('user');
    
    if ($auth->isValid()) {
        $user->setRoleId('user');
        $userdata = $auth->getUserdata();
        $user->setUsername($auth->getUsername());
        $user->setFirstName($userdata['first_name']);
        $user->setEmail($userdata['email']);
        
        // specify roles in settings file, else, user
        $roles = $this->get('settings')['roles'] ?? [];
        
        foreach ($roles as $role=>$users) {
            if (in_array($auth->getUsername(), $users)) {
                $user->setRoleId($role);
                break;
            }
        }
    }
    
    $response = $handler->handle($request);
    
    

    return $response;
};
$app->add($userMiddleware);


