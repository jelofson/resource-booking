<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<div class="modal fade" id="bookingModal" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-scrollabl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?=$title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?=$form->id('booking-form')->action($form_action)->begin(); ?>
            <?=$form->getElement('csrf_token'); ?>
            <?=$form->getElement('userid'); ?>
            <input type="hidden" name="returned" value="0" />
            
            <div class="modal-body">
                
                <div class="form-group">
                    <?=$form->getElement('label_title'); ?>
                    <?=$form->getElement('title'); ?>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <?=$form->getElement('label_date_start'); ?>
                            <?=$form->getElement('date_start')->class('date-selector form-control'); ?>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <?=$form->getElement('label_date_end'); ?>
                            <?=$form->getElement('date_end')->class('date-selector form-control'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?php if ($acl->isAllowed($user, 'booking', 'setuser')) : ?>
                        <div class="form-group" style="width: 100%">
                            <?=$form->label('User Lookup'); ?>
                            <?=$form->text()->idName('user')->class('form-control')->attribute('autocomplete','off'); ?>
                        </div>
                        <div class="chip">
                            <name><?=$fullname; ?></name>
                            <button type="button" class="close" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="resources">Resources</label>
                            <?=$form->getElement('resources'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?=$form->submit('Save')->class('btn btn-primary'); ?>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
            <?=$form->end(); ?>
        </div>
    </div>
</div>
<script>
    var options = {
        theme: "bootstrap",
        adjustWidth: false,
        url: function (name) {
            return "<?=$router->urlFor('user-search'); ?>?name=" + name;
        },
        getValue: function (element) {
            return element.text;
        },
        requestDelay: 250,
        placeholder: "Search last name",
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                var data = $("#user").getSelectedItemData();
                console.log(data);
                $("#user").val('');
                $("#userid").val(data.value);
                $(".chip name").text(data.text);
                $('.chip').show();
            }
        }
    };
    $("#user").easyAutocomplete(options);
    $(".chip .close").on('click', function () {
        $(this).parent('.chip').hide();
        $("#userid").val('');
    });

</script>
