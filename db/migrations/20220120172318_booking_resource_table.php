<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class BookingResourceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        if ( ! $this->hasTable('booking_resource')) {
            $table = $this->table('booking_resource');
            $table->addColumn('booking_id', 'integer')
                  ->addColumn('resource_id', 'integer')
                  ->addIndex('booking_id')
                  ->addIndex('resource_id')
                  ->create();
                  
            $table->addForeignKey('booking_id', 'booking', 'id')
                  ->addForeignKey('resource_id', 'resource', 'id')
                  ->update();
        }
    }
}
