<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

namespace ResourceBooking\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Container\ContainerInterface;
use Vespula\Event\EventHandlerTrait;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpBadRequestException;

/**
 * Base controller from which to extend
 *
 * @author jon.elofson@gmail.com
 *
 */
abstract class Controller 
{
    use EventHandlerTrait;
    
    /**
     * The PSR-11 container interface
     * @var \Psr\Container\ContainerInterface
     */
    protected $container;

    /**
     * The PSR 7 request
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    protected $request;

    /**
     * The PSR 7 response
     * @var \Psr\Http\Message\ResponseInterface
     */
    protected $response;

    /**
     * Messages that can be passed to the view
     *
     * @var array
     */
    protected $messages = [
        'info'=>[],
        'error'=>[]
    ];

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    public function __construct(ContainerInterface $container, ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->container = $container;
        $this->request = $request;
        $this->response = $response;

        $this->registerEvents();

        // Check auth status
        if ($this->auth->isIdle()) {
            $this->addInfoMessage('You are no logger logged in. Reason: idle');
        } else if ($this->auth->isExpired()) {
            $this->addInfoMessage('You are no logger logged in. Reason: session expired');
        } else if ($this->auth->isValid()) {

            //$this->user->setUsername($this->auth->getUsername());
            //$this->user->setFullname($this->auth->getUserdata('fullname'));
        }

        $flashInfo = $this->getFlash('info');
        $flashError = $this->getFlash('error');

        $this->addInfoMessage($flashInfo);
        $this->addErrorMessage($flashError);
    }


    /**
     * Magic method to retrive properties of the class, or services in the $services array if they exist
     *
     * @param string $property The property name
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        if ($this->container->has($property)) {
            return $this->container->get($property);
        }

        trigger_error('Undefined property ' . $property . ' in ' . get_class($this));
    }

    /**
     * Set a 404 not found status with an appropriate view template.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function notFound()
    {
        throw new HttpNotFoundException($this->request, 'Item Not Found');
    }

    /**
     * Set a 403 not allowed status with an appropriate view template.
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function notAllowed()
    {
        throw new HttpForbiddenException($this->request, 'You are not allowed to perform this action');
    }

    /**
     * Set a 400 bad request status with an appropriate view template.
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws HttpBadRequestException
     */
    protected function badRequest($note = null)
    {
        $message = 'Bad Request';
        if ($note) {
            $message .= ' ' . $note;
        }
        throw new HttpBadRequestException($this->request, $message);

    }

    /**
     * Redirect to a differnet url
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function redirect($url, $status = 301)
    {
        return $this->response->withStatus($status)->withHeader('Location', $url);
    }

    protected function registerEvents()
    {
        // Override
    }

    /**
     * Get an aura session flash message by key and segment
     *
     * @param string $key The flash name
     * @param string $segmentName The session segment name. Defaults to the base controller class
     * @return string The flash message
     */
    protected function getFlash($key, $segmentName = null)
    {
        if (! $segmentName) {
            $segmentName = get_parent_class($this);
        }

        $segment = $this->session->getSegment($segmentName);
        return $segment->getFlash($key);

    }

    /**
     * Set a flash message in session
     *
     * @param string $key The flash name
     * @param string $message The flash message
     * @param string $segmentName The session segment. Defaults to the base controller class
     */
    protected function setFlash($key, $message, $segmentName = null)
    {
        if (! $segmentName) {
            $segmentName = get_parent_class($this);
        }

        $segment = $this->session->getSegment($segmentName);
        $segment->setFlash($key, $message);
    }

    /**
     * Add a string message to the `info` key in the messages array
     *
     * @param string $message
     */
    protected function addInfoMessage($message)
    {
        if ($message) {
            $this->messages['info'][] = $message;
        }
    }

    /**
     * Add a string message to the `error` key in the messages array
     *
     * @param string $message
     */
    protected function addErrorMessage($message)
    {
        if ($message) {
            $this->messages['error'][] = $message;
        }
    }

    protected function isXhr()
    {
        return $this->request->getHeaderLine('X-Requested-With') === 'XMLHttpRequest';
    }
}
