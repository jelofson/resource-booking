<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

$acl = $container->get('acl');
$acl->addResource('index');
$acl->addResource('booking');
$acl->addResource('resource');
$acl->addResource('user');

$acl->addRole('guest');
$acl->addRole('user', 'guest');
$acl->addRole('admin', 'user');

$acl->allow('admin');

// Modify these up as needed.
$acl->allow('guest', null, 'browse');
$acl->allow('guest', 'resource', 'json');
$acl->allow('guest', null, 'read');
$acl->allow('user', 'booking', 'add');
$acl->allow('user', 'booking', 'edit', new \ResourceBooking\Acl\Assertion\BookingOwnerAssertion());
$acl->allow('user', 'booking', 'return', new \ResourceBooking\Acl\Assertion\BookingOwnerAssertion());




