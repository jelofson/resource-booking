<?php
session_start();
require '../vendor/autoload.php';

use Slim\Factory\AppFactory;
use League\Container\Container;

// Include the settings file
$settings = require '../src/ResourceBooking/settings.php';

// Get the container
$container = new Container;

AppFactory::setContainer($container);
$app = AppFactory::create();
$app->setBasePath($settings['base_uri']);

$routeParser = $app->getRouteCollector()->getRouteParser();
$container->add('router', $routeParser);

// Get dependencies
include '../src/ResourceBooking/dependencies.php';

// include access control list rules
include '../src/ResourceBooking/acl.php';

// Get middleware
include '../src/ResourceBooking/middleware.php';

// include project routes
include '../src/ResourceBooking/routes.php';

$htmlErrorRenderer = new ResourceBooking\Error\Renderers\HtmlErrorRenderer($container->get('view'));

$errorMiddleware = $app->addErrorMiddleware(
    $settings['displayErrorDetails'], 
    $settings['logErrors'], 
    $settings['logErrorDetails'],
    $container->get('log')
);
$errorHandler = $errorMiddleware->getDefaultErrorHandler();
$errorHandler->registerErrorRenderer('text/html', $htmlErrorRenderer);

// Run app
$app->run();
