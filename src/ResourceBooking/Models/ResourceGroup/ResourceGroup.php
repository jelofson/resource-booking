<?php

/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

declare(strict_types=1);

namespace ResourceBooking\Models\ResourceGroup;

use Atlas\Mapper\Mapper;
use Atlas\Table\Row;

/**
 * @method ResourceGroupTable getTable()
 * @method ResourceGroupRelationships getRelationships()
 * @method ResourceGroupRecord|null fetchRecord($primaryVal, array $with = [])
 * @method ResourceGroupRecord|null fetchRecordBy(array $whereEquals, array $with = [])
 * @method ResourceGroupRecord[] fetchRecords(array $primaryVals, array $with = [])
 * @method ResourceGroupRecord[] fetchRecordsBy(array $whereEquals, array $with = [])
 * @method ResourceGroupRecordSet fetchRecordSet(array $primaryVals, array $with = [])
 * @method ResourceGroupRecordSet fetchRecordSetBy(array $whereEquals, array $with = [])
 * @method ResourceGroupSelect select(array $whereEquals = [])
 * @method ResourceGroupRecord newRecord(array $fields = [])
 * @method ResourceGroupRecord[] newRecords(array $fieldSets)
 * @method ResourceGroupRecordSet newRecordSet(array $records = [])
 * @method ResourceGroupRecord turnRowIntoRecord(Row $row, array $with = [])
 * @method ResourceGroupRecord[] turnRowsIntoRecords(array $rows, array $with = [])
 */
class ResourceGroup extends Mapper
{
}
