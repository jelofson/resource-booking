<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */
?>

<?php $this->layout('layouts::' . $theme); ?>

<?=$this->alerts($messages); ?>

<h2>Browse <?=$record_type; ?> Records</h2>

<p>[<a href="<?=$router->urlFor($add_route_name); ?>">Add new</a>]</p>

<div style="margin-bottom: 20px;">
<?php foreach ($records as $record) : ?>
    <table class="table table-bordered">
        <?php foreach ($columns as $col) : ?>
        <tr>
            <th><?=$col; ?></th>
            <td><?=$record->$col; ?></td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <th></th>
            <td><a href="<?=$router->urlFor($read_route_name, [$record->getPrimaryCol()=>$record->getPrimaryVal()]); ?>">View Full Record</a></td>
        </tr>
    </table>
    <hr style="border: 2px dashed black"/>
<?php endforeach; ?>
</div>
<?=$pagination; ?>