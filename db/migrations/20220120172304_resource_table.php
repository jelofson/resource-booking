<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

final class ResourceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        if ( ! $this->hasTable('resource')) {
            $table = $this->table('resource');
            $table->addColumn('resource', 'string', ['limit'=>150])
                  ->addColumn('resource_group_id', 'integer')
                  ->addColumn('model', 'string', ['limit'=>150, 'null'=>true])
                  ->addColumn('serial_number', 'string', ['limit'=>150, 'null'=>true])
                  ->addColumn('location', 'string', ['limit'=>150])
                  ->addColumn('description', 'text', ['limit'=>MysqlAdapter::TEXT_LONG, 'null'=>true])
                  ->addColumn('image', 'string', ['limit'=>150, 'null'=>true])
                  ->addColumn('current_mileage', 'string', ['limit'=>50, 'null'=>true])
                  ->addColumn('service_mileage', 'string', ['limit'=>50, 'null'=>true])
                  ->addColumn('service_date', 'date', ['limit'=>50, 'null'=>true])
                  ->addColumn('available', 'integer', ['limit'=>1, 'signed'=>false, 'default'=>1])
                  ->addIndex('resource_group_id')
                  ->create();

            $table->addForeignKey('resource_group_id', 'resource_group', 'id')->update();
        }
    }
}
