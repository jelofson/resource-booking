<?php
/**
 * This file is part of CFS Resource Booking.
 * 
 * @copyright Copyright 2021, Natural Resource Canada, Canadian Forest Service
 * @author Jon Elofson <jon.elofson@nrcan-rncan.gc.ca>
 * @license http://www.gnu.org/licenses/gpl-3.0.html 
 * 
 *
 * CFS Resource Booking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CFS Resource Booking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CFS Resource Booking. If not, see <https://www.gnu.org/licenses/>.
 */

return [
    'env'=>'development',
    'displayErrorDetails'=>true,
    'logErrors'=>true,
    'logErrorDetails'=>true,
    'title'=>'Resource Booking',
    'theme'=>'bootstrap',
    'base_uri'=>'',
    'port'=>80,
    'scheme'=>'http',
    'network_policy_string'=>'By using this system you are agreeing to the <a href="%s">NRCan Policy on Electronic Networks</a>',
    'network_policy_url'=>'https://intranet.nrcan.gc.ca/nrcan-policy-use-electronic-networks',
    'atlas'=>[
        'pdo'=>[
            'mysql:dbname=yourdb;host=localhost;charset=utf8',
            'juser',
            '********'
        ],
        'namespace'=>'ResourceBooking\\Models',
        'directory'=>'src/ResourceBooking/Models',
        'templates' => 'src/ResourceBooking/Models/AtlasTemplates'
    ],
    'form'=>[
        'default_classes'=>[
            'text'=>'form-control',
            'textarea'=>'form-control',
            'select'=>'form-control',
        ],
        'blacklist'=>[
            'id'
        ]
    ],
    'roles'=>[
        'admin'=>[
            'juser'
        ]
    ],
    'authAdapter'=>'authAdapterSql',
    'mailTransport'=>'mailTransportFile',
    'email_save_path'=>'/tmp',
    'email_from'=>'your.email@gmail.com',
    'employee_api_urls'=>[
        'search'=>'http://192.168.0.200:8001/employees/search',
        'record'=>'http://192.168.0.200:8001/employees/%s'
    ],

    'ldap_uri'=>'SomeServer.org',
    'ldap_dn'=>'CN=%s,OU=Users,OU=Foo,DC=org',
    'bindOptions'=> [
        'basedn'=>'OU=Users,DC=Foo,DC=org',
        'binddn'=>'CN=someuser,OU=Users,OU=Foo,DC=org',
        'bindpw'=>'********',
        'filter'=>'cn=%s'
    ],
    // Users required for the initial user seeding (for resources)
    'user_seeder_users'=>[
        'juser',
    ],
    'user_seeder_admin'=>'juser'
];